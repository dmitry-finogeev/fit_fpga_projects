onbreak {quit -f}
onerror {quit -f}

vsim -t 1ps -lib xil_defaultlib CDM_Clk_pll_opt

do {wave.do}

view wave
view structure
view signals

do {CDM_Clk_pll.udo}

run -all

quit -force
