----------------------------------------------------------------------------------
-- Company: INR RAS Moscow
-- Engineer: Finogeev D.A. (dmitry-finogeev@yandex.ru)
-- 
-- Create Date:    14:00:14 12/22/2016 
-- Design Name: FIT - GBT project
-- Module Name:    FIT_GBT_kc705_designe - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use ieee.std_logic_arith.all;

library unisim;
use unisim.vcomponents.all;

use work.fit_gbt_common_package.all;


entity FIT_TESTMODULE_v2 is
	Port(
		RESET : 		in  STD_LOGIC;
		
		SYS_CLK_P : 	in  STD_LOGIC;
		SYS_CLK_N : 	in  STD_LOGIC;
		USER_CLK_P : 	in  STD_LOGIC;
		USER_CLK_N : 	in  STD_LOGIC;
		SMA_MGT_CLK_P :	in  STD_LOGIC;
		SMA_MGT_CLK_N :	in  STD_LOGIC;
		
        eth_clk_p: in std_logic; -- 125MHz MGT clock
        eth_clk_n: in std_logic;
		
		SFP_RX_P : 		in  STD_LOGIC;
		SFP_RX_N : 		in  STD_LOGIC;
		SFP_TX_P : 		out  STD_LOGIC;
		SFP_TX_N : 		out  STD_LOGIC;
		SFP_TX_DSBL : 	out  STD_LOGIC;
		

		
		GPIO_SMA_J13 : 	out  STD_LOGIC;
		GPIO_SMA_J14 : 	out  STD_LOGIC;
		
		GPIO_LED_0 : 	out  STD_LOGIC;
		GPIO_LED_1 : 	out  STD_LOGIC;
		GPIO_LED_2 : 	out  STD_LOGIC;
		GPIO_LED_3 : 	out  STD_LOGIC;
		GPIO_LED_4 : 	out  STD_LOGIC;
		GPIO_LED_5 : 	out  STD_LOGIC;
		GPIO_LED_6 : 	out  STD_LOGIC;
		GPIO_LED_7 : 	out  STD_LOGIC;
		GPIO_BUTTON_SW_C: in STD_LOGIC;
		
		-- FTM V1.0
		LAS_EN : out STD_LOGIC;
        LAS_D_P : out STD_LOGIC;
        LAS_D_N : out STD_LOGIC;

		FMC_HPC_clk_A_p :	in  STD_LOGIC;
		FMC_HPC_clk_A_n :	in  STD_LOGIC;
		FMC_HPC_clk_200_p :	in  STD_LOGIC;
		FMC_HPC_clk_200_n :	in  STD_LOGIC;

        eth_rx_p: in std_logic; -- Ethernet MGT input
        eth_rx_n: in std_logic;
        eth_tx_p: out std_logic; -- Ethernet MGT output
        eth_tx_n: out std_logic;
		
        sfp_los: in std_logic;
        sfp_rate_sel: out std_logic_vector(1 downto 0); -- SFP rate select
		
        spi_ss: out std_logic;
        spi_mosi: out std_logic;
        spi_miso: in std_logic;
        spi_sclk: out std_logic
		
		);
end FIT_TESTMODULE_v2;



architecture Behavioral of FIT_TESTMODULE_v2 is


-- generators cloks
	signal SYSCLK_gen : std_logic;
	signal SMA_MGT_CLK : std_logic;	
	signal USERCLK_gen : std_logic;
	signal source_gen : std_logic;
	
-- CDM clocks
	signal CDM_clk_A : std_logic;
	signal CDM_clk_200 : std_logic;
	signal CDM_pll_SysClk : std_logic;
	signal CDM_pll_clk_A : std_logic;

-- FIT PM clocks
	signal SysClk_pll : std_logic;   
	signal DataClk_pll : std_logic;   
	signal MgtRefClk_pll : std_logic; 
	
-- CLOCK to FIT_GBT
	signal SysClk_to_FIT_GBT : std_logic;   
	signal DataClk_to_FIT_GBT : std_logic;
	signal MgtRefClk_to_FIT_GBT : std_logic;   
	signal GBT_RxFrameClk	: STD_LOGIC;
	
	
-- GBT signals
	signal	GBTRX_IsData_rxclk_signal 	:  STD_LOGIC;
	signal	GBTRX_Data_rxclk_signal 	:  std_logic_vector(GBT_data_word_bitdepth-1 downto 0);
		
	signal	GBTTX_IsData_dataclk_signal :  STD_LOGIC;
	signal	GBTTX_Data_dataclk_signal 	:  std_logic_vector(GBT_data_word_bitdepth-1 downto 0);
	
	signal from_gbt_bank_prj_GBT_status : Type_GBT_status;
	
	
-- IP-BUS signals
	signal ipb_clk, ipb_rst : std_logic;
	signal ipb_data_in, ipb_data_out : STD_LOGIC_VECTOR (31 downto 0);
	signal ipb_addr : STD_LOGIC_VECTOR(11 downto 0);
	signal ipb_iswr, ipb_isrd, ipb_ack, ipb_err : std_logic;

	
-- TEST Module signals
	signal FSM_Clocks_signal : FSM_Clocks_type;
    signal TESTM_status : FIT_GBT_status_type;
	signal Laser_Signal_out : std_logic;

	
	attribute keep : string;
	attribute keep of GBTRX_IsData_rxclk_signal : signal is "true";
	attribute keep of GBTRX_Data_rxclk_signal : signal is "true";
	attribute keep of GBTTX_IsData_dataclk_signal : signal is "true";
	attribute keep of GBTTX_Data_dataclk_signal : signal is "true";
	
	attribute keep of from_gbt_bank_prj_GBT_status : signal is "true";
	attribute keep of Laser_Signal_out : signal is "true";
	

	
	
-- TESTs
	signal Data_Clk_strobe : STD_LOGIC;
	

   COMPONENT PmClockPll PORT(
      RESET: in std_logic;
      CLK_IN1_200: in std_logic;
      CLK_OUT1_200: out std_logic;
      CLK_OUT2_40: out std_logic;
      CLK_OUT3_320: out std_logic
   );
   END COMPONENT;
   
   COMPONENT CDM_Clk_pll PORT(
      RESET: in std_logic;
      CLK_IN1_40: in std_logic;
      CLK_OUT1_40: out std_logic;
      CLK_OUT2_320: out std_logic
   );
   END COMPONENT;

   
  
   
   
   
begin
-- wiring ==============================================
-- PLL clocking source
source_gen <= USERCLK_gen;

-- CLOCK to FIT_GBT
-- SysClk_to_FIT_GBT <= SysClk_pll;   
-- DataClk_to_FIT_GBT <= DataClk_pll;   
-- MgtRefClk_to_FIT_GBT <= MgtRefClk_pll;   

SysClk_to_FIT_GBT <= CDM_pll_SysClk;   
--DataClk_to_FIT_GBT <= CDM_pll_clk_A;
DataClk_to_FIT_GBT <= CDM_clk_A;
MgtRefClk_to_FIT_GBT <= CDM_clk_200;   

FSM_Clocks_signal.Reset <= RESET;
FSM_Clocks_signal.Data_Clk <= DataClk_to_FIT_GBT;
FSM_Clocks_signal.System_Clk <= SysClk_to_FIT_GBT;
FSM_Clocks_signal.System_Counter <= (others => '0');
FSM_Clocks_signal.GBT_RX_Clk <= GBT_RxFrameClk;
FSM_Clocks_signal.IPBUS_Data_Clk <= ipb_clk;

		
-- USER OUTPUTS
GPIO_LED_0 <= from_gbt_bank_prj_GBT_status.rxWordClkReady_from_gbtExmplDsgn; -- from rxPgaseAlign_gen.rxBitSlipControl
GPIO_LED_1 <= from_gbt_bank_prj_GBT_status.rxFrameClkReady_from_gbtExmplDsgn; -- from latOpt_phalgnr_gen.phase_conm_inst
-- in xlx_k7v7_mgt_latopt       MGT_O.mgtLink(i).ready           <= txfsm_reset_done(i) and rxfsm_reset_done(i) and rxResetDone_r3(i) and txResetDone_r2(i);
GPIO_LED_2 <= from_gbt_bank_prj_GBT_status.mgtLinkReady_from_gbtExmplDsgn; -- from FitGbtPrg/gbtBankDsgn/gbtExmplDsgn_inst/gbtBank/mgt_param_package_src_gen.mgt/mgtLatOpt_gen.mgtLatOpt/gtxLatOpt_gen[1].xlx_k7v7_mgt_std_i/U0/gt0_txresetfsm_i
GPIO_LED_3 <= from_gbt_bank_prj_GBT_status.gbtRx_Ready_from_gbt_ExmplDsgn; -- FitGbtPrg/gbtBankDsgn/gbtExmplDsgn_inst/gbtBank/gbtRx_param_package_src_gen.gbtRx_gen[1].gbtRx/status/statusLatOpt_gen.RX_READY_O_reg
GPIO_LED_4 <= from_gbt_bank_prj_GBT_status.gbtTx_Ready_from_gbt_ExmplDsgn; -- <= '1'; --from_gbtBank_gbtTx(1).ready; JM: To be modified
GPIO_LED_5 <= from_gbt_bank_prj_GBT_status.mgt_cplllock_from_gbtExmplDsgn; -- CPLLLOCK from FitGbtPrg/gbtBankDsgn/gbtExmplDsgn_inst/gbtBank/mgt_param_package_src_gen.mgt/mgtLatOpt_gen.mgtLatOpt/gtxLatOpt_gen[1].xlx_k7v7_mgt_std_i/U0/xlx_k7v7_mgt_ip_i/gt0_xlx_k7v7_mgt_ip_i/gtxe2_i 
GPIO_LED_6 <= from_gbt_bank_prj_GBT_status.tx_resetDone_from_gbtExmplDsgn; -- TXRESETDONE from gtxe2_i
GPIO_LED_7 <= from_gbt_bank_prj_GBT_status.tx_fsmResetDone_from_gbtExmplDsgn; -- gt0_txresetfsm_i

--GPIO_SMA_J13 <= CDM_clk_A;
--GPIO_SMA_J14 <= CDM_clk_200;
 GPIO_SMA_J13 <=from_gbt_bank_prj_GBT_status.txWordClk_from_gbtExmplDsgn; -- FitGbtPrg/gbtBankDsgn/gbtExmplDsgn_inst/gbtBank/mgt_param_package_src_gen.mgt/mgtLatOpt_gen.mgtLatOpt/txWordClkBufg (drived by TXOUTCLK)
-- GPIO_SMA_J14 <= FIT_GBT_status.GBT_status.txFrameClk_from_gbtExmplDsgn; -- fmc clock A
 GPIO_SMA_J14 <= from_gbt_bank_prj_GBT_status.txOutClkFabric_from_gbtExmplDsgn;-- TXOUTCLKFABRIC from FitGbtPrg/gbtBankDsgn/gbtExmplDsgn_inst/gbtBank/mgt_param_package_src_gen.mgt/mgtLatOpt_gen.mgtLatOpt/gtxLatOpt_gen[1].xlx_k7v7_mgt_std_i/U0/xlx_k7v7_mgt_ip_i/gt0_xlx_k7v7_mgt_ip_i/gtxe2_i

--GPIO_SMA_J13 <= FIT_GBT_status.GBT_status.rxFrameClk_from_gbtExmplDsgn;--
--GPIO_SMA_J14 <= FIT_GBT_status.GBT_status.rxWordClk_from_gbtExmplDsgn;-- 
-- ================================================

 Laser_Signal_out <= '1' when (TESTM_status.Trigger_from_CRU = TRG_const_Ph) else '0';
--Laser_Signal_out <= TESTM_status.Trigger_from_CRU(4);

-- Clocking Buffers & Pll ==============================
-- SYSCLK IBUFGDS 
   sysClockIbufgds: ibufgds
      generic map ( 
         DIFF_TERM => FALSE, -- Differential Termination 
         IBUF_LOW_PWR => FALSE, -- Low power (TRUE) vs. performance (FALSE) setting for referenced I/O standards
         IOSTANDARD                                  => "LVDS_25")
      port map (     
         O                                           => SYSCLK_gen,   
         I                                           => SYS_CLK_P,  
         IB                                          => SYS_CLK_N 
      );

-- USER CLK
   userClockIbufgds: ibufgds
      generic map (
         IBUF_LOW_PWR                                => FALSE,      
         IOSTANDARD                                  => "LVDS_25")
      port map (     
         O                                           => USERCLK_gen,   
         I                                           => USER_CLK_P,  
         IB                                          => USER_CLK_N 
      );

-- FMC HPC CLK A
   CDM_clk_A_Ibufgds: ibufds
     generic map (
		--DIFF_TERM 									=> FALSE,
        IBUF_LOW_PWR                                => FALSE,      
        IOSTANDARD                                  => "LVDS_25")
      port map (     
         O                                           => CDM_clk_A,   
         I                                           => FMC_HPC_clk_A_p,  
         IB                                          => FMC_HPC_clk_A_n 
      );

-- Laser signal
    LAS_EN <= '1';
   Laser_Obufgds: obufds
     generic map (
		--DIFF_TERM 									=> FALSE,
--        IBUF_LOW_PWR                                => FALSE,      
        IOSTANDARD                                  => "LVDS_25")
      port map (     
         I                                           => Laser_Signal_out,   
         O                                           => LAS_D_P,  
         OB                                          => LAS_D_N 
      );



  CDM_clk_200_IbufdsGtxe2: ibufds_gte2
     port map (
        O                                           => CDM_clk_200,
        ODIV2                                       => open,
        CEB                                         => '0',
        I                                           => FMC_HPC_clk_200_p,
        IB                                          => FMC_HPC_clk_200_n
     );
	 
	 

-- IBUFGDS SMA MGT
  smaMgtRefClkIbufdsGtxe2: ibufds_gte2
     port map (
        O                                           => SMA_MGT_CLK,
        ODIV2                                       => open,
        CEB                                         => '0',
        I                                           => SMA_MGT_CLK_P,
        IB                                          => SMA_MGT_CLK_N
     );
	 
	 

-- PLL by KC705 generator 
PmClockPllcomp : PmClockPll
port map(
     RESET  => RESET,
     CLK_IN1_200  => source_gen,
	 CLK_OUT1_200  => MgtRefClk_pll,
	 CLK_OUT2_40  => DataClk_pll,
	 CLK_OUT3_320 => SysClk_pll
);

--  PLL by CDM clock A
CDMClkpllcomp : CDM_Clk_pll
port map(
     RESET  => RESET,
     CLK_IN1_40  => CDM_clk_A,
	 CLK_OUT1_40  => CDM_pll_clk_A,
	 CLK_OUT2_320  => CDM_pll_SysClk
);
-- =====================================================



-- TEST module ===============================================
FIT_TESTMODULE_core_comp: entity work.FIT_TESTMODULE_core port map(

		FSM_Clocks_I 	=> FSM_Clocks_signal,
				
		GBTRX_IsData_rxclk_I => GBTRX_IsData_rxclk_signal,
		GBTRX_Data_rxclk_I => GBTRX_Data_rxclk_signal,
		
		GBTTX_IsData_dataclk_O => GBTTX_IsData_dataclk_signal,
		GBTTX_Data_dataclk_O => GBTTX_Data_dataclk_signal,
		
		TESTM_status_O=>TESTM_status,
		
		IPBUS_rst_I => ipb_rst,
		IPBUS_data_out_O => ipb_data_in,
		IPBUS_data_in_I => ipb_data_out,
		IPBUS_addr_I => ipb_addr,
		IPBUS_iswr_I => ipb_iswr,
		IPBUS_isrd_I => ipb_isrd,
		IPBUS_ack_O => ipb_ack,
		IPBUS_err_O => ipb_err,
		IPBUS_base_addr_I => (others => '0')		
);
-- =============================================================


-- IP-BUS module ===============================================
ipbus_module: entity work.IPBUS_module port map(
        RST_I => RESET,
		
        eth_clk_p => eth_clk_p, --125MHz MGT clock
        eth_clk_n=>eth_clk_n,
        eth_rx_p=>eth_rx_p, -- Ethernet MGT input
        eth_rx_n=>eth_rx_n,
        eth_tx_p=>eth_tx_p, -- Ethernet MGT output
        eth_tx_n=>eth_tx_n,
				
        sfp_los=>sfp_los,
        sfp_rate_sel=>sfp_rate_sel, -- SFP rate select
		
        spi_ss=>spi_ss,
        spi_mosi=>spi_mosi,
        spi_miso=>spi_miso,
        spi_sclk=>spi_sclk,
		
		ipb_clk_o=>ipb_clk,
		ibp_rst_o=>ipb_rst,
		ipb_data_in=>ipb_data_in,
		ipb_data_out=>ipb_data_out,
		ipb_addr_out=>ipb_addr,
		ipb_iswr_o=>ipb_iswr,
		ipb_isrd_o=>ipb_isrd,
		ipb_ack_in=>ipb_ack,
		ipb_err_in=>ipb_err
);
-- =============================================================



-- GBT BANK Designe ===========================================	
	 gbtBankDsgn : entity work.GBT_bank_designe
	 port map (
		RESET_I  => RESET,
		FabricClk_I => DataClk_to_FIT_GBT,
		MgtRefClk_I => MgtRefClk_to_FIT_GBT,
		TXDataClk_I => DataClk_to_FIT_GBT,
		
		SFP_RX_P_I => SFP_RX_P,
		SFP_RX_N_I => SFP_RX_N,
		SFP_TX_P_O => SFP_TX_P,
		SFP_TX_N_O => SFP_TX_N,
		
		TXData_I => GBTTX_Data_dataclk_signal,
		IsTXData_I => GBTTX_IsData_dataclk_signal,
		
		RXDataClk_O => GBT_RxFrameClk,
		RXData_O => GBTRX_Data_rxclk_signal,
		IsRXData_O => GBTRX_IsData_rxclk_signal,
		
		GBT_Status_O => from_gbt_bank_prj_GBT_status
	);
			 
		-- MGT_TX_P_O <= '0';
		-- MGT_TX_N_O <= '0';
		
		-- GBT_RxFrameClk_O <= '0';
		-- RxData_rxclk_from_GBT_O <= (others => '0');
		-- IsRxData_rxclk_from_GBT_O <= '0';
		
		-- from_gbt_bank_prj_GBT_status.txFrameClk_from_gbtExmplDsgn <= '0';
		-- from_gbt_bank_prj_GBT_status.txWordClk_from_gbtExmplDsgn <= '0';
		-- from_gbt_bank_prj_GBT_status.rxFrameClk_from_gbtExmplDsgn <= '0';
		-- from_gbt_bank_prj_GBT_status.rxWordClk_from_gbtExmplDsgn <= '0';
		-- from_gbt_bank_prj_GBT_status.txOutClkFabric_from_gbtExmplDsgn <= '0';
		-- from_gbt_bank_prj_GBT_status.mgt_cplllock_from_gbtExmplDsgn <= '0';
		-- from_gbt_bank_prj_GBT_status.rxWordClkReady_from_gbtExmplDsgn <= '0';
		-- from_gbt_bank_prj_GBT_status.rxFrameClkReady_from_gbtExmplDsgn <= '0';
		-- from_gbt_bank_prj_GBT_status.mgtLinkReady_from_gbtExmplDsgn <= '0';
		-- from_gbt_bank_prj_GBT_status.tx_resetDone_from_gbtExmplDsgn <= '0';
		-- from_gbt_bank_prj_GBT_status.tx_fsmResetDone_from_gbtExmplDsgn <= '0';
		-- from_gbt_bank_prj_GBT_status.gbtRx_Ready_from_gbt_ExmplDsgn <= '0';
		-- from_gbt_bank_prj_GBT_status.gbtTx_Ready_from_gbt_ExmplDsgn <= '0';

		
 -- =============================================================


end Behavioral;

