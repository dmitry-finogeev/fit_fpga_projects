----------------------------------------------------------------------------------
-- Company: INR RAS
-- Engineer: Finogeev D.A. dmitry-finogeev@yandex.ru
-- 
-- Create Date:    10:29:21 08/11/2017 
-- Design Name: 	
-- Module Name:    
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.all;
use ieee.std_logic_unsigned.all ;
use work.fit_gbt_common_package.all;

package fit_gbt_board_package is


	type board_data_type is record
		is_data		: std_logic;
		data_word	: std_logic_vector(GBT_data_word_bitdepth-1 downto 0);
	end record;
	
	constant board_data_test_const : board_data_type :=
	(						
		is_data => '0',
		data_word => (others => '0')
	);

	
-- =============================================================


end fit_gbt_board_package;



package body fit_gbt_board_package is
 -- void
end fit_gbt_board_package;
