----------------------------------------------------------------------------------
-- Company: INR RAS
-- Engineer: Finogeev D.A. dmitry-finogeev@yandex.ru
-- 
-- Create Date:    10:29:21 08/11/2017 
-- Design Name: 	
-- Module Name:    
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.all;
use ieee.std_logic_unsigned.all ;

package fit_gbt_common_package is

-- ===== CONSTANTS =============================================

	constant ENABLED										: integer := 1;
	constant DISABLED										: integer := 0;

-- data size constants -----------------------------------------
	constant GBT_data_word_bitdepth	: integer := 80;
	constant GBT_slowcntr_bitdepth	: integer := 4;
	constant Orbit_id_bitdepth		: integer := 32;
	constant BC_id_bitdepth			: integer := 12;
	constant Trigger_bitdepth		: integer := 32;
	constant rx_phase_bitdepth		: integer := 4;
	constant FEE_ID_bitdepth		: integer := 16;
	constant PAR_bitdepth		    : integer := 16;
	
	constant n_pckt_wrds_bitdepth	: integer := 8;
	constant GEN_count_bitdepth		: integer := 16;
-- -------------------------------------------------------------

	constant GEN_const_void	       : std_logic_vector(GEN_count_bitdepth-1 downto 0) := x"0000";
	constant GEN_const_full	       : std_logic_vector(GEN_count_bitdepth-1 downto 0) := x"ffff";

-- Trigger constants -------------------------------------------
	constant TRG_const_void		: std_logic_vector(Trigger_bitdepth-1 downto 0) := x"00000000";
	constant TRG_const_Orbit	: std_logic_vector(Trigger_bitdepth-1 downto 0) := x"00000001";
	constant TRG_const_HB		: std_logic_vector(Trigger_bitdepth-1 downto 0) := x"00000002";
	constant TRG_const_Ph		: std_logic_vector(Trigger_bitdepth-1 downto 0) := x"00000010";
	constant TRG_const_SOT		: std_logic_vector(Trigger_bitdepth-1 downto 0) := x"00000080";
	constant TRG_const_EOT		: std_logic_vector(Trigger_bitdepth-1 downto 0) := x"00000100";
	constant TRG_const_SOC		: std_logic_vector(Trigger_bitdepth-1 downto 0) := x"00000200";
	constant TRG_const_EOC		: std_logic_vector(Trigger_bitdepth-1 downto 0) := x"00000400";
	constant TRG_const_ORBCrsv	: std_logic_vector(Trigger_bitdepth-1 downto 0) := x"0000000f";
	constant TRG_const_response	: std_logic_vector(Trigger_bitdepth-1 downto 0) := x"ffffffff";
-- -------------------------------------------------------------



-- Experiment constants ----------------------------------------
	constant LHC_BCID_max	       : std_logic_vector(BC_id_bitdepth-1 downto 0) := x"deb";
-- -------------------------------------------------------------


-- DAQ Constants -----------------------------------------------
--constant data_word_cnst_SOP : std_logic_vector(GBT_data_word_bitdepth-1 downto 0) := x"10000000000000000000"; -- SOP CRU
constant data_word_cnst_SOP : std_logic_vector(GBT_data_word_bitdepth-1 downto 0) :=   x"00000000000000000001"; -- SOP G-RORC

--constant data_word_cnst_EOP : std_logic_vector(GBT_data_word_bitdepth-1 downto 0) := x"20000000000000000000"; -- eop CRU
constant data_word_cnst_EOP : std_logic_vector(GBT_data_word_bitdepth-1 downto 0) :=   x"00000000000000000002"; -- eop G-RORC

-- -------------------------------------------------------------


-- FIFO constants ----------------------------------------------
	constant fifo_data_bitdepth		: integer := GBT_data_word_bitdepth;
	constant fifo_depth				: integer := 128;
	constant fifo_count_bitdepth	: integer := 7;
-- -------------------------------------------------------------

-- =============================================================





-- ===== FIT GBT Readout types =================================
	type FSM_Clocks_type is record
		Reset 			: std_logic;
		Data_Clk 		: std_logic;
		System_Clk		: std_logic;
		System_Counter	: std_logic_vector(3 downto 0);
		GBT_RX_Clk		: std_logic;
		IPBUS_Data_Clk	: std_logic;
	end record;
	
-- FSM_Clocks_I.Reset
-- FSM_Clocks_I.Data_Clk
-- FSM_Clocks_I.System_Clk
-- FSM_Clocks_I.System_Counter
-- FSM_Clocks_I.GBT_RX_Clk
-- FSM_Clocks_I.IPBUS_Data_Clk
-- =============================================================




-- ===== CONTROL REGISTER ======================================
-- IPBUS regisrer map see in  file FIT_GBT_IPBUS_control.vhd

	type Type_Gen_use_type is (use_NO_generator, use_MAIN_generator, use_TX_generator);
	type Data_Gen_CONTROL_type is record
		usage_generator : Type_Gen_use_type;
		-- 00000-/n_cycle_void/-XXXX-/n_cycle_data/
		n_cycle_void : std_logic_vector(GEN_count_bitdepth-1 downto 0); -- GBT word between packets
		n_cycle_data : std_logic_vector(GEN_count_bitdepth-1 downto 0); -- GBT packet lenght
	end record;
	
	
	type Type_trgGen_use_type is (use_NO_generator, use_CONST_generator);
	type Readout_command_type is (command_off, send_SOC, send_SOT, send_EOC, send_EOT, test01_RD_mode); -- test01 - RD mode ignored

	type Trigger_Gen_CONTROL_type is record
		usage_generator : Type_trgGen_use_type;
		trigger_rate : std_logic_vector(GEN_count_bitdepth-1 downto 0); -- trigger rate (number of BCs)
		Readout_command : Readout_command_type;
	end record;
	
		
	type CONTROL_REGISTER_type is record
		Data_Gen : Data_Gen_CONTROL_type;
		Trigger_Gen : Trigger_Gen_CONTROL_type;
		n_BCID_delay : std_logic_vector(BC_id_bitdepth-1 downto 0); -- delay between ID from TX and ID in module data not implemented yet
		FEE_ID : std_logic_vector(FEE_ID_bitdepth-1 downto 0);		
		PAR : std_logic_vector(PAR_bitdepth-1 downto 0);		
	end record;
	
	constant test_CONTROL_REG : CONTROL_REGISTER_type :=
	(
		Data_Gen => (
			--usage_generator	=> use_TX_generator,
			usage_generator	=> use_MAIN_generator,
			n_cycle_void 	=> x"fff0", -- 
			n_cycle_data	=> x"fff1" -- = n_cycle_void + n_data
			),
			
		Trigger_Gen => (
			usage_generator	=> use_CONST_generator,
			--usage_generator	=> use_NO_generator
			trigger_rate => x"ffff",
			Readout_command => test01_RD_mode -- test01_RD_mode data send with out mode
			),
			
		n_BCID_delay => x"01f",
		FEE_ID => x"0001",
		PAR => x"ffff"
	);	
-- =============================================================


-- ===== CONTROL REGISTER ======================================
	type TESTMODULE_GBTTX_MUX_type is (CRU_Sim, PM_DATA_Sim);
	type TESTMODULE_GBTRX_MUX_type is (GBT_RX, PM_DATA_Sim);
	
	type TESTMODULE_CNTREG_type is record
		TESTMODULE_GBTTX_MUX : TESTMODULE_GBTTX_MUX_type;
		TESTMODULE_GBTRX_MUX : TESTMODULE_GBTRX_MUX_type;
	end record;
	
	constant test_TESTMODULE_CNTREG_const : TESTMODULE_CNTREG_type :=
	(
		TESTMODULE_GBTTX_MUX => PM_DATA_Sim,
		TESTMODULE_GBTRX_MUX => PM_DATA_Sim
	);
-- =============================================================

-- ===== FIT GBT STATUS ========================================
	type Type_Readout_Mode is (mode_CNT, mode_TRG, mode_IDLE);
	type Type_BCIDsync_Mode is (mode_STR, mode_SYNC, mode_LOST);
	
	type Type_GBT_status is record
	    txFrameClk_from_gbtExmplDsgn            : std_logic;	--reg bit 0
		txWordClk_from_gbtExmplDsgn             : std_logic;	--reg bit 1
		rxFrameClk_from_gbtExmplDsgn            : std_logic;	--reg bit 2
		rxWordClk_from_gbtExmplDsgn             : std_logic;	--reg bit 3
		txOutClkFabric_from_gbtExmplDsgn		: std_logic;	--reg bit 4
		
		mgt_cplllock_from_gbtExmplDsgn			: std_logic;	--reg bit 5

		rxWordClkReady_from_gbtExmplDsgn		: std_logic; 	--reg bit 6
		rxFrameClkReady_from_gbtExmplDsgn       : std_logic; 	--reg bit 7

		mgtLinkReady_from_gbtExmplDsgn 			:std_logic;		--reg bit 8
		tx_resetDone_from_gbtExmplDsgn 			:std_logic;		--reg bit 9
		tx_fsmResetDone_from_gbtExmplDsgn 		:std_logic;		--reg bit a
		
		gbtRx_Ready_from_gbt_ExmplDsgn			:std_logic;		--reg bit b
		gbtTx_Ready_from_gbt_ExmplDsgn			:std_logic;		--reg bit c
	end record;

	
	type FIT_GBT_status_type is record
		GBT_status : Type_GBT_status;
		Readout_Mode : Type_Readout_Mode;
		BCIDsync_Mode : Type_BCIDsync_Mode;
		Trigger_from_CRU : std_logic_vector(Trigger_bitdepth-1 downto 0); -- Trigger ID from CRUS
		BCID_from_CRU : std_logic_vector(BC_id_bitdepth-1 downto 0); -- BC ID from CRUS
		ORBIT_from_CRU : std_logic_vector(Orbit_id_bitdepth-1 downto 0); -- ORBIT from CRUS
		BCID_from_CRU_corrected : std_logic_vector(BC_id_bitdepth-1 downto 0); -- BC ID from CRUS
		ORBIT_from_CRU_corrected : std_logic_vector(Orbit_id_bitdepth-1 downto 0); -- ORBIT from CRUS
		rx_phase : std_logic_vector(rx_phase_bitdepth-1 downto 0);
		FIFO_Data_Count : std_logic_vector(GEN_count_bitdepth-1 downto 0);
	end record;
-- =============================================================


end fit_gbt_common_package;



package body fit_gbt_common_package is
 -- void
end fit_gbt_common_package;
