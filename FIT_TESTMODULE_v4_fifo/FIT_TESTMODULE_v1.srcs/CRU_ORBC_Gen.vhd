----------------------------------------------------------------------------------
-- Company: INR RAS
-- Engineer: Finogeev D. A. dmitry-finogeev@yandex.ru
-- 
-- Create Date:    07/11/2017 
-- Design Name: 
-- Module Name:    RXDATA_CLKSync - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision
-- Additional Comments: 
--
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use ieee.numeric_std.all;
use ieee.std_logic_unsigned.all ;

use work.fit_gbt_common_package.all;
use work.fit_gbt_board_package.all;


entity CRU_ORBC_Gen is
    Port ( 
		FSM_Clocks_I 		: in FSM_Clocks_type;
		
		FIT_GBT_status_I	: in FIT_GBT_status_type;
		Control_register_I	: in CONTROL_REGISTER_type;
				
		RX_IsData_I 		: in STD_LOGIC;
		RX_Data_I 			: in std_logic_vector(GBT_data_word_bitdepth-1 downto 0);
		
		RX_IsData_O 		: out STD_LOGIC;
		RX_Data_O 			: out std_logic_vector(GBT_data_word_bitdepth-1 downto 0);
		
		Current_BCID_from_O	: out std_logic_vector(BC_id_bitdepth-1 downto 0); -- BC ID from CRUS
		Current_ORBIT_from_O: out std_logic_vector(Orbit_id_bitdepth-1 downto 0); -- ORBIT from CRUS
        Current_Trigger_from_O	: out std_logic_vector(Trigger_bitdepth-1 downto 0)
	 );
end CRU_ORBC_Gen;

architecture Behavioral of CRU_ORBC_Gen is

	signal RX_Data_gen_ff, RX_Data_gen_ff_next 			: std_logic_vector(GBT_data_word_bitdepth-1 downto 0);
	signal RX_IsData_gen_ff, RX_IsData_gen_ff_next		: STD_LOGIC;
	
	signal EV_ID_counter_set : std_logic;
	signal EV_ID_counter		: std_logic_vector(Orbit_id_bitdepth + BC_id_bitdepth-1 downto 0);
	signal IS_Orbit_trg_counter : std_logic;

	signal phtrg_counter_ff, phtrg_counter_ff_next : std_logic_vector(GEN_count_bitdepth-1 downto 0); -- physics trigger counter
	
    type readout_trg_type is (trg_idle, trg_SOC, trg_SOT, trg_EOC, trg_EOT);
    signal rd_trg_send_mode, rd_trg_send_mode_next : readout_trg_type;
    signal is_rd_trg_send : std_logic;
    
    signal TRG_physics	: std_logic_vector(Trigger_bitdepth-1 downto 0);
	signal is_phys_trigger_sending : std_logic;
	
    signal is_trigger_sending : std_logic; -- emulating CRU trigger messages
    signal TRG_evid	: std_logic_vector(Trigger_bitdepth-1 downto 0);
    signal TRG_readout_command	: std_logic_vector(Trigger_bitdepth-1 downto 0);
    signal TRG_result	: std_logic_vector(Trigger_bitdepth-1 downto 0);

begin


-- ***************************************************
	RX_Data_O <= RX_Data_I 				WHEN (Control_register_I.Trigger_Gen.usage_generator = use_NO_generator) ELSE RX_Data_gen_ff;
	RX_IsData_O <= RX_IsData_I 			WHEN (Control_register_I.Trigger_Gen.usage_generator = use_NO_generator) ELSE RX_IsData_gen_ff;
-- ***************************************************

Current_BCID_from_O <= EV_ID_counter(BC_id_bitdepth-1 downto 0);
Current_ORBIT_from_O <= EV_ID_counter(Orbit_id_bitdepth + BC_id_bitdepth-1 downto BC_id_bitdepth);
Current_Trigger_from_O <= TRG_result;
-- BC Counter ==================================================
	BC_counter_datagen_comp : entity work.BC_counter
	port map (
		RESET_I			=> FSM_Clocks_I.Reset,
		DATA_CLK_I		=> FSM_Clocks_I.Data_Clk,
		
		IS_INIT_I		=> EV_ID_counter_set,
		ORBC_ID_INIT_I 	=> (others => '0'),
			
		ORBC_ID_COUNT_O => EV_ID_counter,
		IS_Orbit_trg_O	=> IS_Orbit_trg_counter
	);
-- =============================================================


-- Data ff data clk **********************************
	process (FSM_Clocks_I.Data_Clk)
	begin

		IF(rising_edge(FSM_Clocks_I.Data_Clk) )THEN
			IF (FSM_Clocks_I.Reset = '1') THEN
				RX_Data_gen_ff 		<= (others => '0');
				RX_IsData_gen_ff	<= '0';
				phtrg_counter_ff		<= (others => '0');
				rd_trg_send_mode <= trg_idle;
			ELSE
				RX_Data_gen_ff		<= RX_Data_gen_ff_next;
				RX_IsData_gen_ff	<= RX_IsData_gen_ff_next;
				phtrg_counter_ff	 <= phtrg_counter_ff_next;
				rd_trg_send_mode    <= rd_trg_send_mode_next;
			END IF;
		END IF;
		
	end process;
-- ***************************************************



-- ***************************************************



---------- Counters ---------------------------------
phtrg_counter_ff_next <= 	(others => '0') WHEN (FSM_Clocks_I.Reset = '1') ELSE
							(others => '0')	WHEN (phtrg_counter_ff = Control_register_I.Trigger_Gen.trigger_rate) ELSE
							phtrg_counter_ff + 1;

							
---------- CRU TX data gen  -------------------------
TRG_result <= TRG_readout_command or TRG_evid or TRG_physics;
is_trigger_sending <= IS_Orbit_trg_counter or is_rd_trg_send or is_phys_trigger_sending;


TRG_evid <=  TRG_const_Orbit WHEN (IS_Orbit_trg_counter = '1') ELSE
            (others => '0');
        
TRG_physics <=  TRG_const_Ph WHEN (phtrg_counter_ff = Control_register_I.Trigger_Gen.trigger_rate) ELSE
            (others => '0');
		
-- RX data
RX_Data_gen_ff_next <=	(others => '0') WHEN (FSM_Clocks_I.Reset = '1') ELSE
						(others => '0') WHEN (is_trigger_sending = '0') ELSE
	EV_ID_counter(Orbit_id_bitdepth + BC_id_bitdepth-1 downto BC_id_bitdepth) & x"0" & EV_ID_counter(BC_id_bitdepth-1 downto 0) & TRG_result;	

RX_IsData_gen_ff_next <=	'0' WHEN (FSM_Clocks_I.Reset = '1') ELSE
							'0' WHEN (is_trigger_sending = '0') ELSE
							'1';
		

-- Event ID counter start
EV_ID_counter_set <=	'1' WHEN (phtrg_counter_ff < x"0002") and (EV_ID_counter = x"00000000_000") ELSE
						'0';
						
-- Physics trigger emulation
is_phys_trigger_sending <= 	'0' WHEN (FSM_Clocks_I.Reset = '1') ELSE
							'1' WHEN(phtrg_counter_ff = Control_register_I.Trigger_Gen.trigger_rate) ELSE
							'0';

-- Readout trigger send
is_rd_trg_send <=   '1' WHEN (rd_trg_send_mode_next /= rd_trg_send_mode) and (rd_trg_send_mode_next /= trg_idle) ELSE
                    '0';

TRG_readout_command <=  TRG_const_SOT WHEN rd_trg_send_mode_next = trg_SOT ELSE
                        TRG_const_EOT WHEN rd_trg_send_mode_next = trg_EOT ELSE
                        TRG_const_SOC WHEN rd_trg_send_mode_next = trg_SOC ELSE
                        TRG_const_EOC WHEN rd_trg_send_mode_next = trg_EOC ELSE
                        (others => '0');

rd_trg_send_mode_next <= trg_idle WHEN (FSM_Clocks_I.Reset = '1') ELSE
    trg_SOT WHEN ((rd_trg_send_mode = trg_idle) and (Control_register_I.Trigger_Gen.Readout_command = send_SOT)) ELSE
    trg_SOC WHEN ((rd_trg_send_mode = trg_idle) and (Control_register_I.Trigger_Gen.Readout_command = send_SOC)) ELSE
    trg_EOT WHEN ((rd_trg_send_mode = trg_idle) and (Control_register_I.Trigger_Gen.Readout_command = send_EOT)) ELSE
    trg_EOC WHEN ((rd_trg_send_mode = trg_idle) and (Control_register_I.Trigger_Gen.Readout_command = send_EOC)) ELSE
    trg_idle WHEN (Control_register_I.Trigger_Gen.Readout_command = command_off) ELSE
    rd_trg_send_mode;




-- ***************************************************

end Behavioral;

