----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    14:40:15 01/30/2017 
-- Design Name: 
-- Module Name:    TX_Data_Gen - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use ieee.numeric_std.all;
use ieee.std_logic_unsigned.all ;

use work.fit_gbt_common_package.all;
use work.fit_gbt_board_package.all;

entity FIT_TESTMODULE_core is
    Port (
		FSM_Clocks_I 	: in FSM_Clocks_type;
				
		GBTRX_IsData_rxclk_I 	: in STD_LOGIC;
		GBTRX_Data_rxclk_I 	: in std_logic_vector(GBT_data_word_bitdepth-1 downto 0);
		
		GBTTX_IsData_dataclk_O 	: out STD_LOGIC;
		GBTTX_Data_dataclk_O 	: out std_logic_vector(GBT_data_word_bitdepth-1 downto 0);
		
		TESTM_status_O : out FIT_GBT_status_type;
		
		IPBUS_rst_I : in std_logic;
		IPBUS_data_out_O : out STD_LOGIC_VECTOR (31 downto 0);
		IPBUS_data_in_I : in STD_LOGIC_VECTOR (31 downto 0);
		IPBUS_addr_I : in STD_LOGIC_VECTOR(11 downto 0);
		IPBUS_iswr_I : in std_logic;
		IPBUS_isrd_I : in std_logic;
		IPBUS_ack_O : out std_logic;
		IPBUS_err_O : out std_logic;
		IPBUS_base_addr_I : in STD_LOGIC_VECTOR(11 downto 0)
	);
end FIT_TESTMODULE_core;

architecture Behavioral of FIT_TESTMODULE_core is

signal TESTM_control : CONTROL_REGISTER_type;
signal TESTM_status : FIT_GBT_status_type;

signal Module_data_to_gen : board_data_type;
signal Module_data_from_gen : board_data_type;
signal CRU_ORBC_RXData_from_gen : std_logic_vector(GBT_data_word_bitdepth-1 downto 0);
signal CRU_ORBC_IsRXData_from_gen : std_logic;

signal DATA_from_FIFO : std_logic_vector(159 downto 0);
signal FIFO_RDEN : std_logic;

attribute keep : string;
attribute keep of FIFO_RDEN : signal is "true";
attribute keep of DATA_from_FIFO : signal is "true";
attribute keep of TESTM_control : signal is "true";
attribute keep of TESTM_status : signal is "true";

	
   COMPONENT fifo_generator_0 PORT (
    rst : IN STD_LOGIC;
    wr_clk : IN STD_LOGIC;
    rd_clk : IN STD_LOGIC;
    din : IN STD_LOGIC_VECTOR(79 DOWNTO 0);
    wr_en : IN STD_LOGIC;
    rd_en : IN STD_LOGIC;
    dout : OUT STD_LOGIC_VECTOR(159 DOWNTO 0);
    full : OUT STD_LOGIC;
    empty : OUT STD_LOGIC;
    rd_data_count : OUT STD_LOGIC_VECTOR(10 DOWNTO 0);
    wr_rst_busy : OUT STD_LOGIC;
    rd_rst_busy : OUT STD_LOGIC
  );
   END COMPONENT;

   
   	constant testbench_CONTROL_REG_default : CONTROL_REGISTER_type :=
	(
		Data_Gen => (
			--usage_generator	=> use_TX_generator,
			usage_generator	=> use_MAIN_generator,
			n_cycle_void 	=> x"00f0", 
			n_cycle_data	=> x"00f5"  
			--n_cycle_data	=> x"0fff" 
			),
			
		Trigger_Gen => (
			usage_generator	=> use_CONST_generator,
			--usage_generator	=> use_NO_generator,
			trigger_rate => x"0e00",
			Readout_command => command_off
			),
			
		n_BCID_delay => x"00f",
		FEE_ID => x"0001",
		PAR => x"ffff"
	);

	
	
begin

Module_data_to_gen.data_word <= CRU_ORBC_RXData_from_gen;
Module_data_to_gen.is_data <= CRU_ORBC_IsRXData_from_gen;

GBTTX_IsData_dataclk_O <= Module_data_from_gen.is_data;
GBTTX_Data_dataclk_O <= Module_data_from_gen.data_word;

TESTM_status_O <= TESTM_status;

-- DATA GENERATOR =====================================
Module_Data_Gen_comp : entity work.Module_Data_Gen
	
	Port map(
		FSM_Clocks_I 		=> FSM_Clocks_I,
		
		FIT_GBT_status_I	=> TESTM_status,
		Control_register_I	=> TESTM_control,
		
		Board_data_I		=> board_data_test_const,
		Board_data_O		=> Module_data_from_gen
		);		
-- =====================================================


-- CRU ORBC GENERATOR ==================================
CRU_ORBC_Gen_comp : entity work.CRU_ORBC_Gen
	
	Port map(
		FSM_Clocks_I 		=> FSM_Clocks_I,
		
		FIT_GBT_status_I	=> TESTM_status,
		Control_register_I	=> TESTM_control,
		
		RX_IsData_I 		=> '0',
		RX_Data_I 			=> (others => '0'),
		
		RX_IsData_O 		=> CRU_ORBC_IsRXData_from_gen,
		RX_Data_O 			=> CRU_ORBC_RXData_from_gen,
		
		Current_BCID_from_O => TESTM_status.BCID_from_CRU,
		Current_ORBIT_from_O=> TESTM_status.ORBIT_from_CRU,
		Current_Trigger_from_O => TESTM_status.Trigger_from_CRU

		);		
-- =====================================================


-- DATA FIFO ===========================================
fifo_generator_0_comp : fifo_generator_0
  PORT MAP (
    rst 	=> FSM_Clocks_I.Reset,
    wr_clk 	=> FSM_Clocks_I.GBT_RX_Clk,
    rd_clk 	=> FSM_Clocks_I.IPBUS_Data_Clk,
    din 	=> GBTRX_Data_rxclk_I,
    wr_en 	=> GBTRX_IsData_rxclk_I,
    rd_en 	=> FIFO_RDEN,
    dout 	=> DATA_from_FIFO,
    full 	=> open,
    empty 	=> open,
    rd_data_count => TESTM_status.FIFO_Data_Count(10 downto 0),
    wr_rst_busy => open,
    rd_rst_busy => open
  );
  TESTM_status.FIFO_Data_Count(15 downto 11) <= "00000";
-- =====================================================

-- IP-BUS data sender ==================================
-- simulation
--TESTM_control <= testbench_CONTROL_REG_default;
FIT_TESTMODULE_IPBUS_sender_comp : entity work.FIT_TESTMODULE_IPBUS_sender
    Port map(
		FSM_Clocks_I 		=> FSM_Clocks_I,
		
		FIT_GBT_status_I	=> TESTM_status,
		Control_register_O	=> TESTM_control,
--		Control_register_O	=> open,
		
		FIFO_Data_ibclk_I => DATA_from_FIFO,
		FIFO_RDEN_O => FIFO_RDEN,
		
		IPBUS_rst_I => IPBUS_rst_I,
		IPBUS_data_out_O => IPBUS_data_out_O,
		IPBUS_data_in_I => IPBUS_data_in_I,
		IPBUS_addr_I => IPBUS_addr_I,
		IPBUS_iswr_I => IPBUS_iswr_I,
		IPBUS_isrd_I => IPBUS_isrd_I,
		IPBUS_ack_O => IPBUS_ack_O,
		IPBUS_err_O => IPBUS_err_O,
		IPBUS_base_addr_I => IPBUS_base_addr_I
	);
-- =====================================================


end Behavioral;

