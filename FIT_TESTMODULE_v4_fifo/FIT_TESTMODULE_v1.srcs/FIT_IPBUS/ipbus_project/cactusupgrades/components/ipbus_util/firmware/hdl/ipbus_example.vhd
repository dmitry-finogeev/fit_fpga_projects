-- ipbus_example
--
-- selection of different IPBus slaves without actual function,
-- just for performance evaluation of the IPbus/uhal system
--
-- Kristian Harder, March 2014
-- based on code by Dave Newbold, February 2011

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use work.ipbus.all;
use work.ipbus_reg_types.all;
use work.ipbus_decode_tcm_address.all;
use work.dss.all;

entity ipbus_slaves is
	port(
		ipb_clk: in std_logic;
		ipb_rst: in std_logic;
		ipb_in: in ipb_wbus;
		ipb_out: out ipb_rbus;
		spi_ss: out std_logic;
        spi_mosi: out std_logic;
        spi_miso: in std_logic;
        spi_sclk: out std_logic;
        
        wr_bus : out std_logic_vector(31 downto 0);
        rd_bus : in std_logic_vector(31 downto 0);
        addr : out std_logic_vector(11 downto 0);
        wr : out std_logic;
        rd : out std_logic;
        ack : in std_logic;
        err : in std_logic
        );

end ipbus_slaves;

architecture rtl of ipbus_slaves is

	signal ipbw: ipb_wbus_array(N_SLAVES - 1 downto 0);
	signal ipbr: ipb_rbus_array(N_SLAVES - 1 downto 0);
	-- stat_rw - switch; ctrl_rw - leds
	signal ctrl_w, ctrl_rw, stat_rw: ipb_reg_v(15 downto 0);
	signal stat_r : ipb_reg_v(16 downto 0);
	signal ss_v: std_logic_vector(7 downto 0);
	signal mosi, miso, sck, ss : std_logic;

begin

wr_bus<=ipbw(N_SLV_REG_PM).ipb_wdata; ipbr(N_SLV_REG_PM).ipb_rdata<=rd_bus; addr<=ipbw(N_SLV_REG_PM).ipb_addr(11 downto 0); wr<=ipbw(N_SLV_REG_PM).ipb_write and ipbw(N_SLV_REG_PM).ipb_strobe;
rd<=(not ipbw(N_SLV_REG_PM).ipb_write) and ipbw(N_SLV_REG_PM).ipb_strobe; ipbr(N_SLV_REG_PM).ipb_ack<=ack; ipbr(N_SLV_REG_PM).ipb_err<=err;
-- ipbus address decode
		
	fabric: entity work.ipbus_fabric_sel
    generic map(
    	NSLV => N_SLAVES,
    	SEL_WIDTH => IPBUS_SEL_WIDTH)
    port map(
      ipb_in => ipb_in,
      ipb_out => ipb_out,
      sel => ipbus_sel_tcm_address(ipb_in.ipb_addr),
      ipb_to_slaves => ipbw,
      ipb_from_slaves => ipbr
    );


	slave_spi: entity work.ipbus_spi
		port map(
			clk => ipb_clk,
			rst => ipb_rst,
			ipb_in => ipbw(N_SLV_SPI_TCM),
			ipb_out => ipbr(N_SLV_SPI_TCM),
			ss => ss_v,
			mosi => mosi,
			miso => miso,
			sclk => sck
		);
    spi_ss <= ss_v(0);
    spi_sclk <= sck;
    miso <= spi_miso;
    spi_mosi <= mosi; 

end rtl;
