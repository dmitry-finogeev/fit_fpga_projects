-- Top-level design for ipbus demo
--
-- This version is for KC705 eval board, using SFP ethernet interface
--
-- You must edit this file to set the IP and MAC addresses
--
-- Dave Newbold, 23/2/11

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

use work.ipbus.ALL;
use work.dss.all;

entity top is port(
		eth_clk_p: in std_logic; -- 125MHz MGT clock
		eth_clk_n: in std_logic;
		eth_rx_p: in std_logic; -- Ethernet MGT input
		eth_rx_n: in std_logic;
		eth_tx_p: out std_logic; -- Ethernet MGT output
		eth_tx_n: out std_logic;
		sfp_los: in std_logic;
        sfp_rate_sel: out std_logic_vector(1 downto 0); -- SFP rate select
		leds: out std_logic_vector(7 downto 0); -- status LEDs
		gpio_dip_sw: in std_logic_vector(3 downto 0); -- 4 DIP SWITCHES 
		spi_ss: out std_logic;
        spi_mosi: out std_logic;
        spi_miso: in std_logic;
        spi_sclk: out std_logic
		
--		config_csn, config_mosi : out STD_LOGIC;
--        config_miso : in STD_LOGIC
	);

end top;

architecture rtl of top is

	signal clk_ipb, rst_ipb, nuke, soft_rst, userled: std_logic;
	signal mac_addr: std_logic_vector(47 downto 0);
	signal ip_addr: std_logic_vector(31 downto 0);
	signal ipb_out: ipb_wbus;
	signal ipb_in: ipb_rbus;
	
--	signal flash_o_int: spi_mo;
--    signal flash_i_int: spi_mi;
--    signal flash_select: std_logic_vector(1 downto 0);
	
begin

-- Infrastructure

	infra: entity work.kc705_basex_infra
		port map(
			eth_clk_p => eth_clk_p,
			eth_clk_n => eth_clk_n,
			eth_tx_p => eth_tx_p,
			eth_tx_n => eth_tx_n,
			eth_rx_p => eth_rx_p,
			eth_rx_n => eth_rx_n,
			sfp_los => sfp_los,
			clk_ipb_o => clk_ipb,
			rst_ipb_o => rst_ipb,
			nuke => nuke,
			soft_rst => soft_rst,
			leds => leds,
--			gpio_dip_sw => gpio_dip_sw,
			mac_addr => mac_addr,
			ip_addr => ip_addr,
			ipb_in => ipb_in,
			ipb_out => ipb_out
		);
		
--	leds(3) <= userled;
--	leds(2) <= sfp_los;
	sfp_rate_sel(1 downto 0) <= B"00";
		
	mac_addr <= X"020ddba11502"; -- Careful here, arbitrary addresses do not always work
--	ip_addr <= X"898ac9f0"; -- 137.138.201.240
--	ip_addr <= X"c0a80020"; -- 192.168.0.32
--	ip_addr <= X"c0a800c9"; -- 192.168.0.201
	ip_addr <= X"ac144b5f"; -- 172.20.75.95


-- ipbus slaves live in the entity below, and can expose top-level ports
-- The ipbus fabric is instantiated within.

	slaves: entity work.ipbus_example
		port map(
			ipb_clk => clk_ipb,
			ipb_rst => rst_ipb,
			ipb_in => ipb_out,
			ipb_out => ipb_in,
			nuke => nuke,
			soft_rst => soft_rst,
			leds => leds,
            gpio_dip_sw => gpio_dip_sw,
			userled => userled,
		    spi_ss => spi_ss,
            spi_mosi => spi_mosi,
            spi_miso => spi_miso,
            spi_sclk => spi_sclk
--			flash_spi_in  => flash_i_int,
--            flash_spi_out => flash_o_int,
--            flash_select => flash_select
		);

-- Flash SPI selection logic :
--with flash_select select
--    flash_i_int.miso <=     config_miso when "00",
--                            flash_miso  when others;

--with flash_select select
--    config_csn <=   flash_o_int.le when "00",
--                    '1'  when others;

--with flash_select select
--    flash_csn <=    flash_o_int.le when "01",
--                    flash_o_int.le when "10",
--                    '1'  when others;

----    flash_sel <= flash_select;
      
--    flash_clk  <= flash_o_int.clk;

--    config_mosi <= flash_o_int.mosi;
--    flash_mosi  <= flash_o_int.mosi;
    


--   cclk_o: entity work.startup
--   port map(
--      flash_cclk => flash_o_int.clk
--      );

end rtl;
