-- 
-- 2015/05/28: synchronises transition on input with clock, and outputs pulse.

library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.std_logic_arith.all;
use IEEE.std_logic_unsigned.all;

entity command_sync is
    port (	
        clk 	        : in std_logic;
		reset		    : in std_logic;	
		command_in	    : in std_logic;
		command_out	    : out std_logic
	 );
end command_sync ;

architecture rtl of command_sync is

signal command          : std_logic := '0';
signal command_syncd    : std_logic_vector(1 downto 0) := (others => '0');

begin

new_command: process(command_in, reset, command_syncd)
    begin
        if (command_syncd(1) = '1' or reset = '1') then
            command <= '0';
        else
            if rising_edge(command_in) then 
                command <= '1';
            end if;
        end if;
    end process new_command;

command_sync: process(clk,reset)
    begin
    if rising_edge(clk) then 
       command_syncd <= command_syncd(0) & command;
        end if;         
    end process command_sync;

    command_out <= command_syncd(1);

end;


