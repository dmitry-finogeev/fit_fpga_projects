-- controller for SPI interface PLL chips of FTM 
-- 
-- Buffer RAMs are 32 bits wide. The word at outgoing buffer(index) is read and a spi frame sent.
-- Returning data is written into incoming buffer(index), the first word/byte can be discarded. 
-- 'Index' increments until 'transfer_count' words/bytes are transfered.
-- Designer: Richard Staley
-- Date: 26th March. 2015
-- modified:
-- 2015/05/24: Transfer count either in bytes or words, set by BYTE_SPI switch.

library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.std_logic_arith.all;
use IEEE.std_logic_unsigned.all;

entity spi32_8_control is
	generic(
    ADDR_WIDTH: natural;-- size of buffer memory, outgoing + incoming. 
    BYTE_SPI:    boolean := FALSE);
    port (	
        spi_clk 	    : in std_logic;
		reset		    : in std_logic;	
		outgoing_data	: in std_logic_vector( 31 downto 0);
		incoming_data	: out std_logic_vector( 31 downto 0);
		do_spi		    : in std_logic;
		busy		    : out std_logic := '0';
		transfer_count	: in std_logic_vector( ADDR_WIDTH + 1 downto 0);
		ram_ptr		    : out std_logic_vector( ADDR_WIDTH - 2  downto 0);
		ram_write	    : out std_logic := '0';
		clk_en		    : out std_logic := '0';
		cs_n		    : out std_logic := '1';
		mosi		    : out std_logic := '0';
		miso		    : in std_logic := '0'
	 );
end spi32_8_control ;


architecture rtl of spi32_8_control is

constant      MSB_FIRST : boolean := BYTE_SPI;-- FALSE for PLL chips
constant      BIG_FRAME : boolean := BYTE_SPI;-- if set: send inside one big cs_n frame
constant      TRAP_CODE : boolean := not BYTE_SPI;-- if set: trap otp code before PLL

signal run_spi          : std_logic := '0';

type shiftstate is 		( idle, start_frame, read_mem, shift_io, write_mem, end_frame  );
signal sequencer 		: shiftstate := idle;

signal shift_count		: integer range 0 to 32 := 0;
signal shift_register	: std_logic_vector(31 downto 0) := (others => '0');
signal mosi_data	    : std_logic_vector(31 downto 0) := (others => '0');

signal miso_rising      : std_logic;
signal le_int           : std_logic := '0';

signal ram_index_u		: unsigned(ADDR_WIDTH - 1 downto 0) := (others => '0');
signal num_words		: std_logic_vector(ADDR_WIDTH - 1 downto 0);
signal num_32words		: unsigned(ADDR_WIDTH - 1 downto 0);
signal ram_index_slv    : std_logic_vector(ADDR_WIDTH - 1 downto 0) := (others => '0');

constant otp_code       : std_logic_vector(31 downto 0) := x"0000A03F";-- eeprom OTP command

attribute dont_touch : string;
attribute dont_touch of ram_index_u : signal is "true";
attribute dont_touch of num_32words : signal is "true";

alias rem_bytes is transfer_count(1 downto 0);
signal term_le : std_logic := '0';

begin

bit_order: for i in 0 to 31 generate
      mosi_data(i) <= outgoing_data(31-i) when MSB_FIRST else outgoing_data(i);
      incoming_data(i) <= shift_register(31-i) when MSB_FIRST else     shift_register(i);
    end generate;


synch: entity work.command_sync
    port map (	
        clk     	     => spi_clk,
		reset		     => reset,
		command_in	     => do_spi,
		command_out	     => run_spi
	 );

-------------------------------------------------------------------------------
-- state machine transitions
-------------------------------------------------------------------------------

spi_sequencer: process(spi_clk,reset,sequencer,run_spi,shift_count)
    begin
    if rising_edge(spi_clk) then 
    
        shift_count <= 0;
        ram_index_u <= ram_index_u;
        
        if reset = '1' then
            sequencer <= idle; 
        else         
            
        case sequencer is 	
            when idle =>
                ram_index_u <= (others => '0');
                if (run_spi = '1' ) then 
                    sequencer <= start_frame; 
                end if;
                busy <= '0';
            when start_frame =>  -- check if any (more) words to send
                if (ram_index_u = num_32words) then 
                    sequencer <= idle;
                else 
                    sequencer <= read_mem;
                end if;
                busy <= '1';
            when read_mem => 
               sequencer <= shift_io;
            when shift_io =>
                shift_count <= shift_count + 1;
                if (shift_count = 31 ) then 
                    sequencer <= write_mem;
                end if;
            when write_mem => 
                sequencer <= end_frame;
            when end_frame => 
               sequencer <= start_frame;
               ram_index_u <= ram_index_u + 1;
            when others => 
                    sequencer <= idle; 
        end case;
    end if;
        
        miso_rising <= miso;
        
    end if;
    
    end process spi_sequencer;

    ram_index_slv <= std_logic_vector(ram_index_u);

    ram_ptr <= ram_index_slv( ADDR_WIDTH - 2  downto 0);

-------------------------------------------------------------------------------
-- serdes input / output shift register
-------------------------------------------------------------------------------

serdes: process(spi_clk,shift_register)
    begin
        
    if falling_edge(spi_clk) then 
    
         ram_write <= '0';
         shift_register  <= shift_register; 
         
        case sequencer is 
            when read_mem => -- and trap PLL eeprom OTP instruction. substitute zero
                if (TRAP_CODE and outgoing_data = otp_code) then
                   shift_register <= (others => '0');
                else
                   shift_register <= mosi_data;
                end if; 
            when shift_io | write_mem => --  needs one extra shift in to grab last bit
                shift_register  <= miso_rising & shift_register (31 downto 1) ; 
            when others => null;
        end case;
        
        if sequencer = write_mem then ram_write <= '1'; end if;
        
        mosi <= shift_register (0);
    
    end if;
    end process serdes;

-------------------------------------------------------------------------------
-- chip and clock enable options
-------------------------------------------------------------------------------

gen_enable_words: if not BYTE_SPI generate

    num_32words <= unsigned(transfer_count( ADDR_WIDTH - 1 downto 0));

chip_enable: process(spi_clk,shift_register)
    begin
        
    if falling_edge(spi_clk) then 
            
        le_int <= '0'; 
        clk_en <= '0';
             
        case sequencer is 
            when shift_io => 
                le_int <= '1';  
                clk_en <= '1';
            when others => null;
        end case;
    
    end if;
    end process chip_enable;
end generate;

-------------------------------------------------------------------------------

gen_enable_bytes: if BYTE_SPI generate

    with rem_bytes select 
    num_32words <=  unsigned(transfer_count( ADDR_WIDTH + 1 downto 2))     when "00",
                    unsigned(transfer_count( ADDR_WIDTH + 1 downto 2)) + 1 when others;

chip_enable: process(spi_clk,shift_register)
    begin
        
    if falling_edge(spi_clk) then 
            
        le_int <= '0'; 
        clk_en <= '0';
             
        case sequencer is 
            when idle =>
                term_le <= '0';
            when start_frame | read_mem | write_mem  | end_frame  => 
                if (term_le = '0' ) then le_int <= '1'; end if;
            when shift_io => 
                if (ram_index_u +1 = num_32words) then
                    if ((rem_bytes = "00" ) or
                        (rem_bytes = "01" and shift_count <  8) or 
                        (rem_bytes = "10" and shift_count < 16) or 
                        (rem_bytes = "11" and shift_count < 24)) then 
                            le_int <= '1';  clk_en <= '1'; 
                            term_le <= '1';
                    else
                            le_int <= '0';  clk_en <= '0';-- continue shifting to align bits
                    end if;
                 else
                    le_int <= '1';  clk_en <= '1';
                end if;
            when others => null;
        end case;
    
    end if;
    end process chip_enable;

end generate;
-------------------------------------------------------------------------------

   cs_n   <= not le_int;

end;


