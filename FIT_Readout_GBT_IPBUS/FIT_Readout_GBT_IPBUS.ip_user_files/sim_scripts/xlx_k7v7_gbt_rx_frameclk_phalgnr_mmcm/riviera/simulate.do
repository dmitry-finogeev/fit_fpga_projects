onbreak {quit -force}
onerror {quit -force}

asim -t 1ps +access +r +m+xlx_k7v7_gbt_rx_frameclk_phalgnr_mmcm -L xil_defaultlib -L xpm -L unisims_ver -L unimacro_ver -L secureip -O5 xil_defaultlib.xlx_k7v7_gbt_rx_frameclk_phalgnr_mmcm xil_defaultlib.glbl

do {wave.do}

view wave
view structure

do {xlx_k7v7_gbt_rx_frameclk_phalgnr_mmcm.udo}

run -all

endsim

quit -force
