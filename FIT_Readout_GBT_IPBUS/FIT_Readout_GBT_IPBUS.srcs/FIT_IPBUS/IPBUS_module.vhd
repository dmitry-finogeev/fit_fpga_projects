----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 11.10.2018 15:55:10
-- Design Name: 
-- Module Name: IPBUS_module - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;
use IEEE.NUMERIC_STD.ALL;

library unisim;
use unisim.vcomponents.all;

library UNISIM;
use UNISIM.VComponents.all;

library work;
use work.ipbus.ALL;
use work.dss.all;
use work.ipbus_reg_types.all;



entity IPBUS_module is
    Port (
        RST_I : in STD_LOGIC;
		
        eth_clk_p: in std_logic; -- 125MHz MGT clock
        eth_clk_n: in std_logic;
        eth_rx_p: in std_logic; -- Ethernet MGT input
        eth_rx_n: in std_logic;
        eth_tx_p: out std_logic; -- Ethernet MGT output
        eth_tx_n: out std_logic;
        sfp_los: in std_logic;
        sfp_rate_sel: out std_logic_vector(1 downto 0); -- SFP rate select
        spi_ss: out std_logic;
        spi_mosi: out std_logic;
        spi_miso: in std_logic;
        spi_sclk: out std_logic;
		
		ipb_clk_o: out std_logic;
		ibp_rst_o: out std_logic;
		ipb_data_in: in STD_LOGIC_VECTOR (31 downto 0);
		ipb_data_out: out STD_LOGIC_VECTOR (31 downto 0);
		ipb_addr_out: out STD_LOGIC_VECTOR(11 downto 0);
		ipb_iswr_o: out std_logic;
		ipb_isrd_o: out std_logic;
		ipb_ack_in: in std_logic;
		ipb_err_in: in std_logic
	);
end IPBUS_module;

architecture Behavioral of IPBUS_module is

signal clk_ipb, rst_ipb, userled: std_logic;
signal mac_addr: std_logic_vector(47 downto 0);
signal ip_addr: std_logic_vector(31 downto 0);
signal ipb_out: ipb_wbus;
signal ipb_in: ipb_rbus;

signal ipbus_di, ipbus_do  : STD_LOGIC_VECTOR (31 downto 0);
signal ipbus_addr :  STD_LOGIC_VECTOR(11 downto 0);
signal ipbus_rd, ipbus_wr, ipbus_ack, ipbus_err :  STD_LOGIC;


begin

sfp_rate_sel(1 downto 0) <= B"00";
mac_addr <= X"020ddba11502"; -- Careful here, arbitrary addresses do not always work
ip_addr <= X"ac144b5f"; -- 172.20.75.95

	ipb_clk_o <= clk_ipb;
	ibp_rst_o <= rst_ipb;
	ipbus_di <= ipb_data_in;
	ipb_data_out <= ipbus_do;
	ipb_addr_out <= ipbus_addr;
	ipb_iswr_o <= ipbus_wr;
	ipb_isrd_o <= ipbus_rd;
	ipbus_ack <= ipb_ack_in;
	ipbus_err <= ipb_err_in;

		
infra: entity work.kc705_basex_infra port map(
    eth_clk_p => eth_clk_p,
    eth_clk_n => eth_clk_n,
    eth_tx_p => eth_tx_p,
    eth_tx_n => eth_tx_n,
    eth_rx_p => eth_rx_p,
    eth_rx_n => eth_rx_n,
    
    clk_ipb_o => clk_ipb,
    rst_ipb_o => rst_ipb,
   
    sfp_los => sfp_los,
    
    nuke => RST_I,
    soft_rst => RST_I,
    
    leds => open, -- status LEDs
    mac_addr => mac_addr,
    
    ip_addr => ip_addr,
    ipb_in => ipb_in,
    ipb_out => ipb_out
);

slaves: entity work.ipbus_slaves
port map(
    ipb_clk => clk_ipb,
    ipb_rst => rst_ipb,
    ipb_in => ipb_out,
    ipb_out => ipb_in,
	
    spi_ss => spi_ss,
    spi_mosi => spi_mosi,
    spi_miso => spi_miso,
    spi_sclk => spi_sclk,
    
    wr_bus=>ipbus_do,
	rd_bus=>ipbus_di,
	addr=>ipbus_addr,
	wr=>ipbus_wr,
	rd=>ipbus_rd,
	ack=>ipbus_ack,
	err =>ipbus_err
	);


end Behavioral;
