#not used
# create_clock -period 25.000 -name MCLKA -waveform {0.000 12.500} [get_ports CLKA_P]

# Ethernet RefClk (125MHz)
create_clock -period 8.000 -name eth_refclk [get_ports eth_clk_p]

create_generated_clock -name clk_ipb -source [get_pins ipbus_module/infra/clocks/mmcm/CLKIN1] [get_pins ipbus_module/infra/clocks/mmcm/CLKOUT1]
create_generated_clock -name eth_clk_125 -source [get_pins ipbus_module/infra/eth/mmcm/CLKIN1] [get_pins ipbus_module/infra/eth/mmcm/CLKOUT2]

create_generated_clock -name decoupled_clk -source [get_pins ipbus_module/infra/eth/decoupled_clk_reg/C] -divide_by 2 [get_pins ipbus_module/infra/eth/decoupled_clk_reg/Q]

# Ethernet driven by Ethernet txoutclk (i.e. via transceiver)
create_generated_clock -name eth_clk_62_5 -source [get_pins ipbus_module/infra/eth/mmcm/CLKIN1] [get_pins ipbus_module/infra/eth/mmcm/CLKOUT1]
create_generated_clock -name eth_clk_125 -source [get_pins ipbus_module/infra/eth/mmcm/CLKIN1] [get_pins ipbus_module/infra/eth/mmcm/CLKOUT2]

# Clocks derived from MMCM driven by Ethernet RefClk directly (i.e. not via transceiver)
create_generated_clock -name clk_ipb -source [get_pins ipbus_module/infra/clocks/mmcm/CLKIN1] [get_pins ipbus_module/infra/clocks/mmcm/CLKOUT1]

set_clock_groups -asynchronous -group [get_clocks -include_generated_clocks eth_refclk] -group [get_clocks -include_generated_clocks ipbus_module/infra/eth/phy/*/gtxe2_i/TXOUTCLK] -group [get_clocks -include_generated_clocks ipbus_module/infra/eth/phy/*/gtxe2_i/RXOUTCLK]

set_false_path -through [get_nets ipbus_module/infra/clocks/nuke_i]
set_false_path -through [get_pins ipbus_module/infra/clocks/rst_reg/Q]

#set_property LOC GTXE2_CHANNEL_X0Y12 [get_cells -hier -filter name=ipbus_module/infra/eth/*/gtxe2_i]


#registers
#--set_false_path -to [get_ports {FIT_GBT_IPBUS_control/test_leds_reg_clka[?]}]
