library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use ieee.numeric_std.all;

Library UNISIM;
use UNISIM.vcomponents.all;

entity startup is
	port(
		flash_cclk: in std_logic
	);
	
end startup;

architecture rtl of startup is

signal logic0,logic1 : std_logic;

begin

   logic0 <= '0';
   logic1 <= '1';

-- STARTUPE2: STARTUP Block
-- 7 Series
-- Xilinx HDL Libraries Guide, version 14.4
   STARTUPE2_inst : STARTUPE2
   generic map (
   PROG_USR => "FALSE", -- Activate program event security feature. Requires encrypted bitstreams.
   SIM_CCLK_FREQ => 0.0 -- Set the Configuration Clock Frequency(ns) for simulation.
   )
   port map (
      CFGCLK => open, -- 1-bit output: Configuration main clock output
      CFGMCLK => open, -- 1-bit output: Configuration internal oscillator clock output
      EOS => open, -- 1-bit output: Active high output signal indicating the End Of Startup.
      PREQ => open, -- 1-bit output: PROGRAM request to fabric output
      CLK => logic0, -- 1-bit input: User start-up clock input
      GSR => logic0, -- 1-bit input: Global Set/Reset input (GSR cannot be used for the port name)
      GTS => logic0, -- 1-bit input: Global 3-state input (GTS cannot be used for the port name)
      KEYCLEARB => logic0, -- 1-bit input: Clear AES Decrypter Key input from Battery-Backed RAM (BBRAM)
      PACK => logic0, -- 1-bit input: PROGRAM acknowledge input
      USRCCLKO => flash_cclk, -- 1-bit input: User CCLK input
      USRCCLKTS => logic0, -- 1-bit input: User CCLK 3-state enable input
      USRDONEO => logic1, -- 1-bit input: User DONE pin output control
      USRDONETS => logic1 -- 1-bit input: User DONE 3-state enable output
   );

end rtl;
