-- 07/05/2015
-- Created by rjs

LIBRARY ieee;
USE ieee.std_logic_1164.all;

ENTITY clock_pulse IS
   PORT( 
      CLK_I      : IN     std_logic;
      RESET      : IN     std_logic;
      Enable     : IN     std_logic;
      pulse      : OUT    std_logic
   );

END clock_pulse ;

------------------------------------
ARCHITECTURE rtl OF clock_pulse IS


   SIGNAL ClockR  : std_logic := '0';
   SIGNAL ClockF  : std_logic := '0';
  
BEGIN

	process(clk_i,reset,enable)
	begin
	if rising_edge(clk_i) then
		if reset = '1' then
		  ClockR <= '0';
		else
            if Enable = '1' then
				ClockR  <= not ClockR;
			end if;
		end if;
	end if;
	end process;


	process(clk_i,reset )
	begin
	if falling_edge(clk_i) then
		if reset = '1' then
            ClockF <= '0';
        else
			ClockF  <= ClockR;
		end if;
	end if;
	end process;

	pulse <= ClockF  xor ClockR;

END rtl;


