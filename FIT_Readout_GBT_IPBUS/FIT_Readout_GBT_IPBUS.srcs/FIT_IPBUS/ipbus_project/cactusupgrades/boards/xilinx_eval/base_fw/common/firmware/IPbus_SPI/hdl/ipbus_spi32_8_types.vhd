-- ipbus memory + SPI controller for PLL chips on FTM 

-- Designer: Richard Staley
-- Date: 26th March. 2015

-- module: ipbus_spi32.vhd
-- ADDR_WIDTH defines RAM size = (# of 32 bit words) / 2
-- do_spi is rising-edge sensitive input
-- 2015/05/24: Transfer count now in bytes for FLASH, RAM is 32 bits wide
-- 2015/05/16: Used modified ipbus_fabric_simple to decode group of slaves

library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.std_logic_arith.all;
use IEEE.std_logic_unsigned.all;

use work.ipbus.all;
use work.dss.all;

entity ipbus_spi32 is
	generic(
      BYTE_SPI:  boolean := FALSE;-- SPI i/o, default is PLL, set TRUE for FLASH, 
      ADDR_WIDTH: natural -- = RAM address width, covers in and out buffer memory
      );
port (
		ipbus_clk: in std_logic;
		reset: in std_logic;
		ipb_in: in ipb_wbus;
		ipb_out: out ipb_rbus;
		spi_in    : in spi_mi;
--		miso		: in std_logic;
		spi_out   : out spi_mo;
--		cs_n		: out std_logic;
--		clko		: out std_logic;
--		mosi		: out std_logic;
		selreg      : out  std_logic_vector(1 downto 0)
	 );

attribute keep_hierarchy : string;
attribute keep_hierarchy of ipbus_spi32 : entity is "yes";

end ipbus_spi32 ;

architecture rtl of ipbus_spi32 is

signal pointer_addr: std_logic_vector(ADDR_WIDTH - 1 downto 0);
signal ram_ptr : std_logic_vector(ADDR_WIDTH - 2 downto 0);
signal ram_write: std_logic;

signal outgoing_data, incoming_data: std_logic_vector(31 downto 0);

--constant  DIV_BITS: natural := 4; 
constant  DIV_BITS: natural := 2;  

signal spi_clk: std_logic := '0';
signal clk_en: std_logic := '0';

constant NSLV: positive := 2;
signal ipbw: ipb_wbus_array(NSLV-1 downto 0);
signal ipbr, ipbr_d: ipb_rbus_array(NSLV-1 downto 0);

signal spi_ctrl: std_logic_vector(127 downto 0);
signal spi_stat: std_logic_vector(31 downto 0);

signal transfer_count : std_logic_vector( ADDR_WIDTH + 1 downto 0);
signal do_spi,busy	:  std_logic;

attribute PRESERVE_SIGNAL : boolean;
attribute PRESERVE_SIGNAL of spi_clk: signal is true;

begin

block_decode: entity work.ipbus_fabric_branch
    generic map(
    NSLV => NSLV,
    DECODE_BASE => ADDR_WIDTH
    )
	port map(
        ipb_in => ipb_in,
        ipb_out => ipb_out,
        ipb_to_slaves => ipbw,
        ipb_from_slaves => ipbr
    );

clock_div:	process(reset, ipbus_clk)
	variable div : std_logic_vector(DIV_BITS downto 1);
	begin
	if reset = '1' then
		div:=(others => '0');
	elsif rising_edge(ipbus_clk) then
		spi_clk <= div(DIV_BITS);
		div:=div+1;
	end if;
	end process;

control_reg: entity work.ipbus_ctrlreg
	generic map(
		ctrl_addr_width => 2,
		stat_addr_width => 0
		)
	port map(
		clk => ipbus_clk,
		reset => reset,
		ipbus_in => ipbw(0),
		ipbus_out => ipbr(0),
		d => spi_stat,
		q => spi_ctrl
	);

	selreg   <= spi_ctrl(1 downto 0);              -- base address + 0
	transfer_count <= spi_ctrl((ADDR_WIDTH + 1) + 64  downto 64); -- 2
	do_spi	 <= spi_ctrl(96);                                     -- 3
	
	spi_stat <= X"0000000" & "000" & busy;                        -- 4
	

spi_dpram: entity work.ipbus_dpram
	generic map(
		ADDR_WIDTH => ADDR_WIDTH
	   )
	port map(
		clk => ipbus_clk,
		rst => reset,
		ipb_in => ipbw(1),
		ipb_out => ipbr(1),
		rclk => spi_clk,
		we => ram_write,
		d => incoming_data,
		q => outgoing_data, 
		addr => pointer_addr
	);


control: entity  work.spi32_8_control
	generic map(
        ADDR_WIDTH => ADDR_WIDTH,
        BYTE_SPI => BYTE_SPI
        )
    port map (
      spi_clk => spi_clk,
      reset => reset,
      outgoing_data => outgoing_data,
      incoming_data => incoming_data,
      do_spi => do_spi,
      busy => busy,
      transfer_count  => transfer_count,
      ram_ptr  => ram_ptr,
      ram_write => ram_write,-- grab data returning from slave
      cs_n => spi_out.le,
      clk_en => clk_en,
      mosi => spi_out.mosi,
      miso => spi_in.miso
    );

--    A clean version of clko <= clk_en and spi_clk;
gen_clock: ENTITY work.clock_pulse
   PORT map( 
      CLK_I => spi_clk ,
      RESET =>  reset,
      Enable =>  clk_en, 
      pulse => spi_out.clk
   );

	pointer_addr <= ram_write & ram_ptr;
	-- incoming block located directly after outgoing block


end rtl;

