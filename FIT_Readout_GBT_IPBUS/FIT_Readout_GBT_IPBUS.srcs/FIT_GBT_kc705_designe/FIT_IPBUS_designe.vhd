----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 09.10.2018 16:16:10
-- Design Name: 
-- Module Name: FIT_IPBUS_designe - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;
use IEEE.NUMERIC_STD.ALL;

library unisim;
use unisim.vcomponents.all;

library UNISIM;
use UNISIM.VComponents.all;

library work;
use work.ipbus.ALL;
use work.dss.all;
use work.ipbus_reg_types.all;


entity FIT_IPBUS_designe is
    Port ( 
        CLKA_P : in STD_LOGIC;
        CLKA_N : in STD_LOGIC;
        
        RST_I : in STD_LOGIC;
                
        GPIO_LED_0 :     out  STD_LOGIC;
        GPIO_LED_1 :     out  STD_LOGIC;
        GPIO_LED_2 :     out  STD_LOGIC;
        GPIO_LED_3 :     out  STD_LOGIC;
        GPIO_LED_4 :     out  STD_LOGIC;
        GPIO_LED_5 :     out  STD_LOGIC;
        GPIO_LED_6 :     out  STD_LOGIC;
        GPIO_LED_7 :     out  STD_LOGIC;
        
--        GPIO_SW_C : in std_logic;
        
        
        -- ---------- IP-BUS ----------
        eth_clk_p: in std_logic; -- 125MHz MGT clock
        eth_clk_n: in std_logic;
        eth_rx_p: in std_logic; -- Ethernet MGT input
        eth_rx_n: in std_logic;
        eth_tx_p: out std_logic; -- Ethernet MGT output
        eth_tx_n: out std_logic;
        sfp_los: in std_logic;
        sfp_rate_sel: out std_logic_vector(1 downto 0); -- SFP rate select
        spi_ss: out std_logic;
        spi_mosi: out std_logic;
        spi_miso: in std_logic;
        spi_sclk: out std_logic
);
          
end FIT_IPBUS_designe;

architecture Behavioral of FIT_IPBUS_designe is

signal CLKA : std_logic;

signal test_leds_reg_clka: std_logic_vector(7 downto 0);

signal test_reg_clk_ipb: std_logic_vector(31 downto 0);
signal test_reg_addr :  STD_LOGIC_VECTOR(11 downto 0);


-- ---- IP-BUS
signal clk_ipb, rst_ipb, userled: std_logic;
signal mac_addr: std_logic_vector(47 downto 0);
signal ip_addr: std_logic_vector(31 downto 0);
signal ipb_out: ipb_wbus;
signal ipb_in: ipb_rbus;

signal ipbus_di, ipbus_do  : STD_LOGIC_VECTOR (31 downto 0);
signal ipbus_addr :  STD_LOGIC_VECTOR(11 downto 0);
signal ipbus_rd, ipbus_wr, ipbus_ack, ipbus_err :  STD_LOGIC;


begin

sfp_rate_sel(1 downto 0) <= B"00";
mac_addr <= X"020ddba11502"; -- Careful here, arbitrary addresses do not always work
ip_addr <= X"ac144b5f"; -- 172.20.75.95
test_reg_addr <= X"010";


GPIO_LED_0 <= test_leds_reg_clka(0);
GPIO_LED_1 <= test_leds_reg_clka(1);
GPIO_LED_2 <= test_leds_reg_clka(2);
GPIO_LED_3 <= test_leds_reg_clka(3);
GPIO_LED_4 <= test_leds_reg_clka(4);
GPIO_LED_5 <= test_leds_reg_clka(5);
GPIO_LED_6 <= test_leds_reg_clka(6);
GPIO_LED_7 <= test_leds_reg_clka(7);

	
CLKA1: IBUFDS
generic map (DIFF_TERM => TRUE, IBUF_LOW_PWR => FALSE, IOSTANDARD => "LVDS_25")
port map (I=>CLKA_P, IB=>CLKA_N, O=>CLKA);

-- IP-BUS register ***********************************
	PROCESS (clk_ipb)
	BEGIN
		IF(clk_ipb'EVENT and clk_ipb = '1') THEN
			IF(RST_I = '1') THEN
				test_reg_clk_ipb <= (others => '0');
				ipbus_di <= (others => '0');
				ipbus_ack <= '0';
				ipbus_err <= '0';
			ELSIF(ipbus_wr = '1') THEN
				test_reg_clk_ipb <= ipbus_do;
				ipbus_di <= (others => '0');
				ipbus_ack <= '1';
				ipbus_err <= '0';
			ELSIF(ipbus_rd = '1') THEN
				ipbus_di <= test_reg_clk_ipb;
				ipbus_ack <= '1';
				ipbus_err <= '0';
			ELSE
				ipbus_di <= (others => '0');
				ipbus_ack <= '0';
				ipbus_err <= '0';
			END IF;
		END IF;
	END PROCESS;
-- ***************************************************

-- LEDs register *************************************
	PROCESS (CLKA)
	BEGIN
		IF(clk_ipb'EVENT and clk_ipb = '1') THEN
			IF(RST_I = '1') THEN
				test_leds_reg_clka <= (others => '0');
			ELSE
				test_leds_reg_clka <= test_reg_clk_ipb(7 downto 0);
			END IF;
		END IF;
	END PROCESS;
-- ***************************************************



infra: entity work.kc705_basex_infra port map(
    eth_clk_p => eth_clk_p,
    eth_clk_n => eth_clk_n,
    eth_tx_p => eth_tx_p,
    eth_tx_n => eth_tx_n,
    eth_rx_p => eth_rx_p,
    eth_rx_n => eth_rx_n,
    
    clk_ipb_o => clk_ipb,
    rst_ipb_o => rst_ipb,
   
    sfp_los => sfp_los,
    
    nuke => RST_I,
    soft_rst => RST_I,
    
    leds => open, -- status LEDs
    mac_addr => mac_addr,
    
    ip_addr => ip_addr,
    ipb_in => ipb_in,
    ipb_out => ipb_out
);

slaves: entity work.ipbus_slaves
port map(
    ipb_clk => clk_ipb,
    ipb_rst => rst_ipb,
    ipb_in => ipb_out,
    ipb_out => ipb_in,
	
    spi_ss => spi_ss,
    spi_mosi => spi_mosi,
    spi_miso => spi_miso,
    spi_sclk => spi_sclk,
    
    wr_bus=>ipbus_do,
	rd_bus=>ipbus_di,
	addr=>ipbus_addr,
	wr=>ipbus_wr,
	rd=>ipbus_rd,
	ack=>ipbus_ack,
	err =>ipbus_err
	);
	
--	ipbus_ack <= '1';
	
	


end Behavioral;
