----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 11.10.2018 15:55:10
-- Design Name: 
-- Module Name: FIT_GBT_IPBUS_control - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;
use IEEE.NUMERIC_STD.ALL;

library unisim;
use unisim.vcomponents.all;

library UNISIM;
use UNISIM.VComponents.all;

library work;
use work.ipbus.ALL;
use work.dss.all;
use work.ipbus_reg_types.all;

use work.fit_gbt_common_package.all;

entity FIT_GBT_IPBUS_control is
    Port (
		DataClk_I 	: in  STD_LOGIC; -- 40MHz data clock
		IPBUS_clk_I : in std_logic;
		IPBUS_rst_I : in std_logic;
		IPBUS_data_out_O : out STD_LOGIC_VECTOR (31 downto 0);
		IPBUS_data_in_I : in STD_LOGIC_VECTOR (31 downto 0);
		IPBUS_addr_I : in STD_LOGIC_VECTOR(11 downto 0);
		IPBUS_iswr_I : in std_logic;
		IPBUS_isrd_I : in std_logic;
		IPBUS_ack_O : out std_logic;
		IPBUS_err_O : out std_logic;

		IPBUS_base_addr_I : in STD_LOGIC_VECTOR(11 downto 0);

		FIT_GBT_status_I : in FIT_GBT_status_type;
		Control_register_O	: out CONTROL_REGISTER_type
	);
end FIT_GBT_IPBUS_control;

architecture Behavioral of FIT_GBT_IPBUS_control is


	
	signal ipbus_di, ipbus_do, ipbus_do_next : STD_LOGIC_VECTOR (31 downto 0);
	signal ipbus_ack, ipbus_err :std_logic;
	
	--   |                DataClk_I                   |                                IPBUS clock			           |
	--  -|> FIT_GBT_status_I -> ipbus_status_reg_map_dc -|-> ipbus_status_reg_ipbclk -> IBBUS  						   |
	--  <|-                 Control_register_reg_dc <-|- Control_register_reg_map_ipbclk <- ipbus_control_reg <- IBBUS |
	
	signal Control_register_reg_map_ipbclk, Control_register_reg_dc : CONTROL_REGISTER_type;
	type cntreg_type is array (natural range <>) of std_logic_vector(31 downto 0);
	signal	ipbus_control_reg : cntreg_type(0 to 7);
	signal	ipbus_status_reg_map, ipbus_status_reg_map_dc, ipbus_status_reg_ipbclk : cntreg_type(0 to 7);
	
	
	-- test debug signals
	signal debug_ipb_clk :std_logic;
	signal debug_ipb_rst :std_logic;
	signal debug_ipb_iswr :std_logic;
	signal debug_ipb_isrd :std_logic;
	signal debug_ipb_ack  :std_logic;
	signal debug_ipb_err  :std_logic;
	signal debug_ipb_data_O : std_logic_vector (31 downto 0);
	signal debug_ipb_data_I :std_logic_vector (31 downto 0);
	signal debug_ipb_addr :std_logic_vector (11 downto 0);
	
	attribute keep : string;
	attribute keep of debug_ipb_clk : signal is "true";
	attribute keep of debug_ipb_rst : signal is "true";
	attribute keep of debug_ipb_iswr : signal is "true";
	attribute keep of debug_ipb_isrd : signal is "true";
	attribute keep of debug_ipb_ack : signal is "true";
	attribute keep of debug_ipb_err : signal is "true";
	attribute keep of debug_ipb_data_O : signal is "true";
	attribute keep of debug_ipb_data_I : signal is "true";
	attribute keep of debug_ipb_addr : signal is "true";

	attribute keep of Control_register_reg_map_ipbclk : signal is "true";
	attribute keep of ipbus_status_reg_ipbclk : signal is "true";
	attribute keep of ipbus_status_reg_map : signal is "true";
	attribute keep of Control_register_reg_dc : signal is "true";

begin

-- debug signal assignement
 debug_ipb_clk <= DataClk_I;
 debug_ipb_rst <= IPBUS_rst_I;
 debug_ipb_iswr <= IPBUS_iswr_I;
 debug_ipb_isrd <= IPBUS_isrd_I;
 debug_ipb_ack  <= ipbus_ack;
 debug_ipb_err  <= ipbus_err;
 debug_ipb_data_O <= ipbus_do;
 debug_ipb_data_I <= IPBUS_data_in_I;
 debug_ipb_addr <= IPBUS_addr_I;


ipbus_di <= IPBUS_data_in_I;
IPBUS_data_out_O <= ipbus_do;
IPBUS_ack_O <= ipbus_ack;
IPBUS_err_O <= ipbus_err;
Control_register_O <= Control_register_reg_dc;



	-- IPBUS regisrer map (32 bits) -----------------
	
	-- control regisrer
	-- M - main gen type: 0 - no gen; 1 -1 main gen; 2 - TX gen
	-- T - trigger gen type: 0 - no gen; 1 - const gen
	-- C - readout command: 0 - command_off, 1 - send_SOC, 2 - send_SOT, 3 - send_EOC, 4 - send_EOT, 5 - test01_RD_mode
	
	-- status regisrer (read only)
	-- U - readout mode 0 - mode_CNT, 1 - mode_TRG, 2 - mode_IDLE
	-- S - BCID sync mode 0 - mode_STR, 1 - mode_SYNC, 2 - mode_LOST
	
	-- control regisrer
	-- 000	XXXX XCTM		X - not used
	-- 001	VVVV DDDD		V - n_cycle_void; D - n_cycle_data (gen data)
	-- 002	XLLL RRRR 	    R - trigger gen rate; L - BCID delay
	-- 003	FFFF PPPP 	F - FEE_ID; P - PAR
	-- 004	XXXX XXXX		
	-- 005	XXXX XXXX 		
	-- 006	XXXX XXXX 		
	-- 007	XXXX XXXX 		
	-- status regisrer (read only)
	-- 0 008	XSPU GGGG 	P - RX phase; G - GBT status
	-- 1 009	TTTT TTTT		T - trigger from CRU
	-- 2 00a	OOOO OOOO		O - Orbit from CRU
	-- 3 00b	oooo oooo		o - Orbit corrected
	-- 4 00c	XBBB Xbbb		B - BC from CRU; b - BC corrected
	-- 5 00d	XXXX XXXX		
	-- 6 00e	XXXX XXXX		
	-- 7 00f	XXXX XXXX		
	-------------------------------------------------

	
Control_register_reg_map_ipbclk.Data_Gen.usage_generator <= use_NO_generator 	WHEN (ipbus_control_reg(0)(3 downto 0) = X"0") ELSE
															use_MAIN_generator	WHEN (ipbus_control_reg(0)(3 downto 0) = X"0") ELSE
															use_TX_generator	WHEN (ipbus_control_reg(0)(3 downto 0) = X"0") ELSE
															use_NO_generator;

Control_register_reg_map_ipbclk.Trigger_Gen.usage_generator <= 	use_NO_generator 	WHEN (ipbus_control_reg(0)(7 downto 4) = X"0") ELSE
																use_CONST_generator	WHEN (ipbus_control_reg(0)(7 downto 4) = X"1") ELSE
																use_NO_generator;
															
Control_register_reg_map_ipbclk.Trigger_Gen.Readout_command <= 	command_off 	WHEN (ipbus_control_reg(0)(11 downto 8) = X"0") ELSE
																send_SOC		WHEN (ipbus_control_reg(0)(11 downto 8) = X"1") ELSE
																send_SOT		WHEN (ipbus_control_reg(0)(11 downto 8) = X"2") ELSE
																send_EOC		WHEN (ipbus_control_reg(0)(11 downto 8) = X"3") ELSE
																send_EOT		WHEN (ipbus_control_reg(0)(11 downto 8) = X"4") ELSE
																test01_RD_mode	WHEN (ipbus_control_reg(0)(11 downto 8) = X"5") ELSE
																command_off;
															
Control_register_reg_map_ipbclk.Data_Gen.n_cycle_data <= ipbus_control_reg(1)(15 downto 0);
Control_register_reg_map_ipbclk.Data_Gen.n_cycle_void <= ipbus_control_reg(1)(31 downto 16);
Control_register_reg_map_ipbclk.Trigger_Gen.trigger_rate <= ipbus_control_reg(2)(15 downto 0);
Control_register_reg_map_ipbclk.n_BCID_delay <= ipbus_control_reg(2)(27 downto 16);
Control_register_reg_map_ipbclk.FEE_ID <= ipbus_control_reg(3)(31 downto 16);
Control_register_reg_map_ipbclk.PAR <= ipbus_control_reg(3)(15 downto 0);


-- ipbus_status_reg_map(0)(0) <= FIT_GBT_status_I.GBT_status.txFrameClk_from_gbtExmplDsgn;
-- ipbus_status_reg_map(0)(1) <= FIT_GBT_status_I.GBT_status.txWordClk_from_gbtExmplDsgn;
-- ipbus_status_reg_map(0)(2) <= FIT_GBT_status_I.GBT_status.rxFrameClk_from_gbtExmplDsgn;
-- ipbus_status_reg_map(0)(3) <= FIT_GBT_status_I.GBT_status.rxWordClk_from_gbtExmplDsgn;
-- ipbus_status_reg_map(0)(4) <= FIT_GBT_status_I.GBT_status.txOutClkFabric_from_gbtExmplDsgn;

ipbus_status_reg_map(0)(0) <= '0';
ipbus_status_reg_map(0)(1) <= '0';
ipbus_status_reg_map(0)(2) <= '0';
ipbus_status_reg_map(0)(3) <= '0';
ipbus_status_reg_map(0)(4) <= '0';

ipbus_status_reg_map(0)(5) <= FIT_GBT_status_I.GBT_status.mgt_cplllock_from_gbtExmplDsgn;
ipbus_status_reg_map(0)(6) <= FIT_GBT_status_I.GBT_status.rxWordClkReady_from_gbtExmplDsgn;
ipbus_status_reg_map(0)(7) <= FIT_GBT_status_I.GBT_status.rxFrameClkReady_from_gbtExmplDsgn;
ipbus_status_reg_map(0)(8) <= FIT_GBT_status_I.GBT_status.mgtLinkReady_from_gbtExmplDsgn;
ipbus_status_reg_map(0)(9) <= FIT_GBT_status_I.GBT_status.tx_resetDone_from_gbtExmplDsgn;
ipbus_status_reg_map(0)(10) <= FIT_GBT_status_I.GBT_status.tx_fsmResetDone_from_gbtExmplDsgn;
ipbus_status_reg_map(0)(11) <= FIT_GBT_status_I.GBT_status.gbtRx_Ready_from_gbt_ExmplDsgn;
ipbus_status_reg_map(0)(12) <= FIT_GBT_status_I.GBT_status.gbtTx_Ready_from_gbt_ExmplDsgn;
ipbus_status_reg_map(0)(13) <= '0';
ipbus_status_reg_map(0)(14) <= '0';
ipbus_status_reg_map(0)(15) <= '0';

ipbus_status_reg_map(0)(19 downto 16) <= 	X"0" WHEN FIT_GBT_status_I.Readout_Mode = mode_CNT ELSE
											X"1" WHEN FIT_GBT_status_I.Readout_Mode = mode_TRG ELSE
											X"2" WHEN FIT_GBT_status_I.Readout_Mode = mode_IDLE ELSE
											X"f";

ipbus_status_reg_map(0)(27 downto 24) <= 	X"0" WHEN FIT_GBT_status_I.BCIDsync_Mode = mode_STR ELSE
											X"1" WHEN FIT_GBT_status_I.BCIDsync_Mode = mode_SYNC ELSE
											X"2" WHEN FIT_GBT_status_I.BCIDsync_Mode = mode_LOST ELSE
											X"f";

ipbus_status_reg_map(0)(23 downto 20) <= FIT_GBT_status_I.rx_phase;
											
											
ipbus_status_reg_map(1) <= FIT_GBT_status_I.Trigger_from_CRU;
ipbus_status_reg_map(2) <= FIT_GBT_status_I.ORBIT_from_CRU;
ipbus_status_reg_map(3) <= FIT_GBT_status_I.ORBIT_from_CRU_corrected;
ipbus_status_reg_map(4)(31 downto 16) <= X"0" & FIT_GBT_status_I.BCID_from_CRU;
ipbus_status_reg_map(4)(15 downto 0) <= X"0" & FIT_GBT_status_I.BCID_from_CRU_corrected;


-- IP-BUS register ***********************************
	PROCESS (DataClk_I)
	BEGIN
		IF(DataClk_I'EVENT and DataClk_I = '1') THEN
		
			IF(IPBUS_rst_I = '1') THEN
				Control_register_reg_dc <= test_CONTROL_REG;
			ELSE
				Control_register_reg_dc <= Control_register_reg_map_ipbclk;
				ipbus_status_reg_map_dc <= ipbus_status_reg_map;
			END IF;
		
		END IF;
	END PROCESS;



-- IP-BUS register ***********************************
	PROCESS (IPBUS_clk_I)
	BEGIN
		IF(IPBUS_clk_I'EVENT and IPBUS_clk_I = '1') THEN
		
			ipbus_status_reg_ipbclk <= ipbus_status_reg_map_dc;
		
			IF(IPBUS_rst_I = '1') THEN
				
			ELSIF(IPBUS_isrd_I = '1') THEN
				
			ELSIF(IPBUS_iswr_I = '1') THEN
				if		(IPBUS_addr_I(11 downto 0) = X"000") THEN ipbus_control_reg(0) <= ipbus_di;
				elsif	(IPBUS_addr_I(11 downto 0) = X"001") THEN ipbus_control_reg(1) <= ipbus_di;
				elsif 	(IPBUS_addr_I(11 downto 0) = X"002") THEN ipbus_control_reg(2) <= ipbus_di;
				elsif 	(IPBUS_addr_I(11 downto 0) = X"003") THEN ipbus_control_reg(3) <= ipbus_di;
				elsif 	(IPBUS_addr_I(11 downto 0) = X"004") THEN ipbus_control_reg(4) <= ipbus_di;
				elsif 	(IPBUS_addr_I(11 downto 0) = X"005") THEN ipbus_control_reg(5) <= ipbus_di;
				elsif 	(IPBUS_addr_I(11 downto 0) = X"006") THEN ipbus_control_reg(6) <= ipbus_di;
				elsif 	(IPBUS_addr_I(11 downto 0) = X"007") THEN ipbus_control_reg(7) <= ipbus_di;
				else 
				end if;				
			ELSE
			
			END IF;
			
		END IF;
	END PROCESS;
-- ***************************************************


-- FSM ***********************************************
ipbus_err <= '0';
ipbus_ack <='0' WHEN (IPBUS_rst_I = '1') ELSE
			'1'		WHEN (IPBUS_addr_I(11 downto 0) = X"000") and ((IPBUS_isrd_I = '1') or (IPBUS_iswr_I = '1')) ELSE
			'1'		WHEN (IPBUS_addr_I(11 downto 0) = X"001") and ((IPBUS_isrd_I = '1') or (IPBUS_iswr_I = '1')) ELSE
			'1'		WHEN (IPBUS_addr_I(11 downto 0) = X"002") and ((IPBUS_isrd_I = '1') or (IPBUS_iswr_I = '1')) ELSE
			'1'		WHEN (IPBUS_addr_I(11 downto 0) = X"003") and ((IPBUS_isrd_I = '1') or (IPBUS_iswr_I = '1')) ELSE
			'1'		WHEN (IPBUS_addr_I(11 downto 0) = X"004") and ((IPBUS_isrd_I = '1') or (IPBUS_iswr_I = '1')) ELSE
			'1'		WHEN (IPBUS_addr_I(11 downto 0) = X"005") and ((IPBUS_isrd_I = '1') or (IPBUS_iswr_I = '1')) ELSE
			'1'		WHEN (IPBUS_addr_I(11 downto 0) = X"006") and ((IPBUS_isrd_I = '1') or (IPBUS_iswr_I = '1')) ELSE
			'1'		WHEN (IPBUS_addr_I(11 downto 0) = X"007") and ((IPBUS_isrd_I = '1') or (IPBUS_iswr_I = '1')) ELSE
			'1'		WHEN (IPBUS_addr_I(11 downto 0) = X"008") and ((IPBUS_isrd_I = '1') or (IPBUS_iswr_I = '1')) ELSE
			'1'		WHEN (IPBUS_addr_I(11 downto 0) = X"009") and ((IPBUS_isrd_I = '1') or (IPBUS_iswr_I = '1')) ELSE
			'1'		WHEN (IPBUS_addr_I(11 downto 0) = X"00a") and ((IPBUS_isrd_I = '1') or (IPBUS_iswr_I = '1')) ELSE
			'1'		WHEN (IPBUS_addr_I(11 downto 0) = X"00b") and ((IPBUS_isrd_I = '1') or (IPBUS_iswr_I = '1')) ELSE
			'1'		WHEN (IPBUS_addr_I(11 downto 0) = X"00c") and ((IPBUS_isrd_I = '1') or (IPBUS_iswr_I = '1')) ELSE
			'1'		WHEN (IPBUS_addr_I(11 downto 0) = X"00d") and ((IPBUS_isrd_I = '1') or (IPBUS_iswr_I = '1')) ELSE
			'1'		WHEN (IPBUS_addr_I(11 downto 0) = X"00e") and ((IPBUS_isrd_I = '1') or (IPBUS_iswr_I = '1')) ELSE
			'1'		WHEN (IPBUS_addr_I(11 downto 0) = X"00f") and ((IPBUS_isrd_I = '1') or (IPBUS_iswr_I = '1')) ELSE
			'0';



ipbus_do <=	(others => '0') WHEN (IPBUS_rst_I = '1') ELSE
			ipbus_control_reg(0)		WHEN (IPBUS_addr_I(11 downto 0) = X"000") and (IPBUS_isrd_I = '1') ELSE
			ipbus_control_reg(1)		WHEN (IPBUS_addr_I(11 downto 0) = X"001") and (IPBUS_isrd_I = '1') ELSE
			ipbus_control_reg(2)		WHEN (IPBUS_addr_I(11 downto 0) = X"002") and (IPBUS_isrd_I = '1') ELSE
			ipbus_control_reg(3)		WHEN (IPBUS_addr_I(11 downto 0) = X"003") and (IPBUS_isrd_I = '1') ELSE
			ipbus_control_reg(4)		WHEN (IPBUS_addr_I(11 downto 0) = X"004") and (IPBUS_isrd_I = '1') ELSE
			ipbus_control_reg(5)		WHEN (IPBUS_addr_I(11 downto 0) = X"005") and (IPBUS_isrd_I = '1') ELSE
			ipbus_control_reg(6)		WHEN (IPBUS_addr_I(11 downto 0) = X"006") and (IPBUS_isrd_I = '1') ELSE
			ipbus_control_reg(7)		WHEN (IPBUS_addr_I(11 downto 0) = X"007") and (IPBUS_isrd_I = '1') ELSE

			ipbus_status_reg_ipbclk(0)		WHEN (IPBUS_addr_I(11 downto 0) = X"008") and (IPBUS_isrd_I = '1') ELSE
			ipbus_status_reg_ipbclk(1)		WHEN (IPBUS_addr_I(11 downto 0) = X"009") and (IPBUS_isrd_I = '1') ELSE
			ipbus_status_reg_ipbclk(2)		WHEN (IPBUS_addr_I(11 downto 0) = X"00a") and (IPBUS_isrd_I = '1') ELSE
			ipbus_status_reg_ipbclk(3)		WHEN (IPBUS_addr_I(11 downto 0) = X"00b") and (IPBUS_isrd_I = '1') ELSE
			ipbus_status_reg_ipbclk(4)		WHEN (IPBUS_addr_I(11 downto 0) = X"00c") and (IPBUS_isrd_I = '1') ELSE
			ipbus_status_reg_ipbclk(5)		WHEN (IPBUS_addr_I(11 downto 0) = X"00d") and (IPBUS_isrd_I = '1') ELSE
			ipbus_status_reg_ipbclk(6)		WHEN (IPBUS_addr_I(11 downto 0) = X"00e") and (IPBUS_isrd_I = '1') ELSE
			ipbus_status_reg_ipbclk(7)		WHEN (IPBUS_addr_I(11 downto 0) = X"00f") and (IPBUS_isrd_I = '1') ELSE
			x"ffff_ffff";


			
			
			
end Behavioral;



