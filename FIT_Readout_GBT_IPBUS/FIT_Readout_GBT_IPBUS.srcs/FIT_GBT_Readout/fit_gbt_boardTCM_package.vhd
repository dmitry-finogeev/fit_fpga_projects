----------------------------------------------------------------------------------
-- Company: INR RAS
-- Engineer: Finogeev D.A. dmitry-finogeev@yandex.ru
-- 
-- Create Date:    10:29:21 08/11/2017 
-- Design Name: 	
-- Module Name:    
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.all;
use ieee.std_logic_unsigned.all ;
use work.fit_gbt_common_package.all;

package fit_gbt_board_package is

	type Board_DataConversion_type_t is (one_word, two_words);

	
-- ===== CONSTANTS =============================================

-- data constants ----------------------------------------------
		constant Board_DataConversion_type	: Board_DataConversion_type_t := one_word;
		constant data_word_bitdepth			: integer := 80; -- max is 80 for TCM, 80/2 for PM
		constant tdwords_bitdepth			: integer := 5;
		constant total_data_words			: integer := 21;
-- -------------------------------------------------------------

-- =============================================================


-- Board data type =============================================
	
	type board_data_words_arr_t is array (natural range <>) of std_logic_vector(data_word_bitdepth-1 downto 0);

	type board_data_type is record
		is_data		: std_logic;
		data_array	: board_data_words_arr_t(0 to total_data_words-1);
	
		ORBC_ID 	: std_logic_vector(Orbit_id_bitdepth+BC_id_bitdepth-1 downto 0); -- BC ID from CRUS
		-- Trigger_ID 	: std_logic_vector(Trigger_bitdepth-1 downto 0); -- Trigger ID from CRUS
		rx_phase 	: std_logic_vector(rx_phase_bitdepth-1 downto 0);
	end record;
	
	constant board_data_test_const : board_data_type :=
		(						
		is_data => '1',
		data_array => (others => x"0123456789abcdef0123"),
	
		ORBC_ID => x"123456789ab",
		-- Trigger_ID => x"12345678",
		rx_phase => x"f"
		);

	
-- =============================================================


end fit_gbt_board_package;



package body fit_gbt_board_package is
 -- void
end fit_gbt_board_package;
