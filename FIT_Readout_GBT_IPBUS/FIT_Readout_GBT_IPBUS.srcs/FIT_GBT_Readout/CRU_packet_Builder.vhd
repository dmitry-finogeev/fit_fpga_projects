----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    15:42:21 04/12/2017 
-- Design Name: 
-- Module Name:    Test_Generator - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--

----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use ieee.numeric_std.all;
use ieee.std_logic_unsigned.all ;

use work.fit_gbt_common_package.all;
use work.fit_gbt_board_package.all;


entity CRU_packet_Builder is
    Port ( 
		FSM_Clocks_I : in FSM_Clocks_type;

		FIT_GBT_status_I : in FIT_GBT_status_type;
		Control_register_I : in CONTROL_REGISTER_type;
		
		FIFO_data_word_I : in std_logic_vector(fifo_data_bitdepth-1 downto 0);
		FIFO_Is_Empty_I : in STD_LOGIC;
		
		FIFO_RE_O : out STD_LOGIC;
		Is_Data_O : out STD_LOGIC;
		Data_O : out std_logic_vector(GBT_data_word_bitdepth-1 downto 0)
	 );
end CRU_packet_Builder;

architecture Behavioral of CRU_packet_Builder is
	
	constant nwords_in_SOP	: integer := 5;
	constant nwords_in_EOP	: integer := 2;

	type SOP_format_type is array (0 to nwords_in_SOP-1) of std_logic_vector(GBT_data_word_bitdepth downto 0);
	type EOP_format_type is array (0 to nwords_in_EOP-1) of std_logic_vector(GBT_data_word_bitdepth downto 0);
    signal SOP_format : SOP_format_type;
    signal EOP_format : EOP_format_type;
	
	signal header_size : std_logic_vector(7 downto 0);
    signal HB_Orbit    : std_logic_vector(Orbit_id_bitdepth-1 downto 0);
    signal HB_BC       : std_logic_vector(BC_id_bitdepth-1 downto 0);
    signal TRG_Orbit   : std_logic_vector(Orbit_id_bitdepth-1 downto 0);
    signal TRG_BC      : std_logic_vector(BC_id_bitdepth-1 downto 0);
	signal TRG_Type    : std_logic_vector(Trigger_bitdepth-1 downto 0);

	
	type FSM_STATE_T is (s0_start, s1_sop, s2_data, s3_eop);
	signal FSM_STATE, FSM_STATE_NEXT  : FSM_STATE_T;
	
	signal Word_Count, Word_Count_next, Words_total : std_logic_vector(n_pckt_wrds_bitdepth-1 downto 0);
	
	signal Data_ff, Data_ff_next : std_logic_vector(GBT_data_word_bitdepth-1 downto 0);
	signal IsData_ff, IsData_ff_next : STD_LOGIC;

	signal dataheader_ff, dataheader_ff_next : std_logic_vector(GBT_data_word_bitdepth-1 downto 0); --header for first event in packets
	

begin

-- Wiring ********************************************
	Is_Data_O <= IsData_ff;
	Data_O <= Data_ff;
-- ***************************************************

-- Data format ***************************************
	Words_total <= dataheader_ff(fifo_data_bitdepth-1 downto fifo_data_bitdepth-n_pckt_wrds_bitdepth);

	header_size <= std_logic_vector(to_unsigned((nwords_in_SOP-1), 8));
	HB_Orbit    <= dataheader_ff(Orbit_id_bitdepth+BC_id_bitdepth-1 downto BC_id_bitdepth);
	HB_BC       <= dataheader_ff(BC_id_bitdepth-1 downto 0);
	TRG_Orbit   <= dataheader_ff(Orbit_id_bitdepth+BC_id_bitdepth-1 downto BC_id_bitdepth);
	TRG_BC      <= dataheader_ff(BC_id_bitdepth-1 downto 0);
	TRG_Type    <= x"00"&dataheader_ff(Orbit_id_bitdepth+BC_id_bitdepth+23 downto Orbit_id_bitdepth+BC_id_bitdepth);

	--             is data
	--SOP_format(0) <= '0' & x"10000000000000000000"; -- SOP CRU
	SOP_format(0) <= '0' & x"00000000000000000001"; -- SOP G-RORC
	--SOP_format(0) <= '0' & data_word_cnst_SOP;
	
	 
	--             is data      extra GBT            header size	Link ID	  FEE ID                      block lenght     header version
	SOP_format(1) <= '1' &      x"0000"&  x"00"&     header_size&   x"01"&    Control_register_I.FEE_ID&  x"ffff"&         x"02";
	
	
	--            is data       extra GBT
	SOP_format(2) <= '1' &      x"0000"&  HB_Orbit &  TRG_Orbit;
	
	
	--             is data      extra GBT            HB BC & HB Orbit
	SOP_format(3) <= '1' &      x"0000"&  x"00"& HB_BC & TRG_Type & TRG_BC;
	
	
	--            is data       extra GBT            PAR 	                   Detector field    stop bit     pages counter
	SOP_format(4) <= '1' &      x"0000"&  x"00"&     Control_register_I.PAR&   x"0000"&         x"01"&       x"0000";
	
	
	EOP_format(0) <= '1' & x"ffffffffffffffffffff"; -- test trailer
	
	--EOP_format(1) <= '0' & x"20000000000000000000"; -- eop CRU
	EOP_format(1) <= '0' & x"00000000000000000002"; -- eop G-RORC
	--EOP_format(1) <= '0' & data_word_cnst_EOP
-- ***************************************************





-- Data clock flip-flops *****************************
	PROCESS (FSM_Clocks_I.Data_Clk)
	BEGIN
		IF(FSM_Clocks_I.Data_Clk'EVENT and FSM_Clocks_I.Data_Clk = '1') THEN
			IF(FSM_Clocks_I.Reset = '1') THEN
				IsData_ff <= '0';
				Data_ff <= (others => '0');
				dataheader_ff <= (others => '0');
				
				FSM_STATE <= s0_start;
				Word_Count <= (others => '0');
			ELSE
				IsData_ff <= IsData_ff_next;
				Data_ff <= Data_ff_next;
				dataheader_ff <= dataheader_ff_next;
				
				FSM_STATE <= FSM_STATE_NEXT;
				Word_Count <= Word_Count_next;
			END IF;
		END IF;
	END PROCESS;
-- ***************************************************




-- FSM ***********************************************
FSM_STATE_NEXT <=	s0_start 	WHEN (FSM_Clocks_I.Reset = '1') ELSE
	s1_sop		WHEN (FSM_STATE = s0_start) and (FIFO_Is_Empty_I = '0') ELSE
	s2_data		WHEN (FSM_STATE = s1_sop) 	and (Word_Count = std_logic_vector(to_unsigned((nwords_in_SOP-1), tdwords_bitdepth))) ELSE
	s3_eop		WHEN (FSM_STATE = s2_data) 	and (Word_Count = Words_total+1) ELSE
	s1_sop		WHEN (FSM_STATE = s3_eop) 	and (Word_Count = std_logic_vector(to_unsigned((nwords_in_EOP-1), tdwords_bitdepth))) and (FIFO_Is_Empty_I = '0') ELSE
	s0_start	WHEN (FSM_STATE = s3_eop) 	and (Word_Count = std_logic_vector(to_unsigned((nwords_in_EOP-1), tdwords_bitdepth))) and (FIFO_Is_Empty_I = '1') ELSE
	FSM_STATE;

	
Word_Count_next <=	(others => '0')	WHEN (FSM_Clocks_I.Reset = '1') ELSE
	(others => '0') WHEN (FSM_STATE = s0_start) 	ELSE
	(others => '0')	WHEN (FSM_STATE = s1_sop) 	and (Word_Count = std_logic_vector(to_unsigned((nwords_in_SOP-1), tdwords_bitdepth))) ELSE
	(others => '0')	WHEN (FSM_STATE = s2_data) 	and (Word_Count = Words_total+1) ELSE
	(others => '0')	WHEN (FSM_STATE = s3_eop) 	and (Word_Count = std_logic_vector(to_unsigned((nwords_in_EOP-1), tdwords_bitdepth))) ELSE
	Word_Count+1;
	
	
dataheader_ff_next <=	(others => '0')	 WHEN (FSM_Clocks_I.Reset = '1') ELSE
						FIFO_data_word_I WHEN (FSM_STATE = s1_sop) and (Word_Count = 0) ELSE
						dataheader_ff;
						
						
IsData_ff_next <= '0'	 													WHEN (FSM_Clocks_I.Reset = '1') ELSE
	SOP_format(to_integer(unsigned(Word_Count)))(GBT_data_word_bitdepth) 	WHEN (FSM_STATE = s1_sop) ELSE
	'1'					 													WHEN (FSM_STATE = s2_data) ELSE
	EOP_format(to_integer(unsigned(Word_Count)))(GBT_data_word_bitdepth)	WHEN (FSM_STATE = s3_eop) ELSE
	'0';

	
Data_ff_next <= (others => '0')	    							                    WHEN (FSM_Clocks_I.Reset = '1') ELSE
	SOP_format(to_integer(unsigned(Word_Count)))(GBT_data_word_bitdepth-1 downto 0)	WHEN (FSM_STATE = s1_sop) ELSE
	FIFO_data_word_I											                    WHEN (FSM_STATE = s2_data) ELSE
	EOP_format(to_integer(unsigned(Word_Count)))(GBT_data_word_bitdepth-1 downto 0)	WHEN (FSM_STATE = s3_eop) ELSE
	(others => '0');

	
FIFO_RE_O <= 	'0'	WHEN (FSM_Clocks_I.Reset = '1') ELSE
--				'1' WHEN (FSM_STATE = s1_sop) and (Word_Count = 0) ELSE
				'1' WHEN (FSM_STATE = s2_data) and (Word_Count <= Words_total+1) ELSE
				'0';

-- ***************************************************
	
	
		
end Behavioral;

