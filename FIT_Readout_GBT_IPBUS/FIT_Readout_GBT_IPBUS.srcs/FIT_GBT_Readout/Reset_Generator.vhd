----------------------------------------------------------------------------------
-- Company: INR RAS
-- Engineer: Finogeev D. A. dmitry-finogeev@yandex.ru
-- 
-- Create Date:    09/11/2017 
-- Design Name: 
-- Module Name:    RXDATA_CLKSync - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision
-- Additional Comments: 
--
----------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use work.fit_gbt_common_package.all;
use ieee.std_logic_unsigned.all ;

entity Reset_Generator is
    Port ( 
		RESET_I 		: in STD_LOGIC;
		SysClk_I 		: in  STD_LOGIC;
		DataClk_I 		: in  STD_LOGIC;
		Sys_Cntr_ready_I: in  STD_LOGIC;
		
		Reset_DClk_O	: out std_logic;
		General_reset_O : out std_logic
		);
end Reset_Generator;

architecture Behavioral of Reset_Generator is

	signal GenRes_DataClk_ff, GenRes_DataClk_ff_next :std_logic;
	signal General_reset_ff, General_reset_ff_next : std_logic;
	signal Cntr_reset_ff, Cntr_reset_ff_next : std_logic;

begin

Reset_DClk_O <= Cntr_reset_ff;
General_reset_O <= GenRes_DataClk_ff;

-- Data clk ***********************************
	PROCESS (SysClk_I)
	BEGIN
		IF rising_edge(SysClk_I)THEN
			IF(RESET_I = '1') THEN
				General_reset_ff <= '1';
				Cntr_reset_ff <= '1';
			ELSE
				General_reset_ff <= General_reset_ff_next;
				Cntr_reset_ff <= Cntr_reset_ff_next;
			END IF;
		END IF;
	END PROCESS;
-- ********************************************

-- Sys clk ***********************************
	PROCESS (DataClk_I)
	BEGIN
		IF rising_edge(DataClk_I)THEN
			IF(RESET_I = '1') THEN
				GenRes_DataClk_ff <= '1';
			ELSE
				GenRes_DataClk_ff <= GenRes_DataClk_ff_next;
			END IF;
		END IF;
	END PROCESS;
-- ********************************************

-- FSM ***********************************************
GenRes_DataClk_ff_next <= '1' WHEN (RESET_I = '1') ELSE
						General_reset_ff;
	
Cntr_reset_ff_next <=	'1'     WHEN (RESET_I = '1') ELSE
--						'1'		WHEN (Cntr_reset_ff = '0') and (Sys_Cntr_ready_I = '0') ELSE
						'0';
						
General_reset_ff_next <= 	'1' WHEN (RESET_I = '1') ELSE
							'1' WHEN (General_reset_ff = '1') and (Sys_Cntr_ready_I = '0') ELSE
							'0';


end Behavioral;

