----------------------------------------------------------------------------------
-- Company: INR RAS
-- Engineer: Finogeev D.A. dmitry-finogeev@yandex.ru
-- 
-- Create Date:    10:29:21 01/09/2017 
-- Design Name: 	FIT GBT
-- Module Name:    FIT_GBT_project - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

use work.all;
use work.fit_gbt_common_package.all;
use work.fit_gbt_board_package.all;

entity FIT_GBT_project is
	generic (
		GENERATE_GBT_BANK	: integer := 1
	);

    Port (		
		RESET_I				: in  STD_LOGIC;
		SysClk_I 			: in  STD_LOGIC; -- 320MHz system clock
		DataClk_I 			: in  STD_LOGIC; -- 40MHz data clock
		MgtRefClk_I 		: in  STD_LOGIC; -- 200MHz ref clock
		RxDataClk_I			: in STD_LOGIC; -- 40MHz data clock in RX domain
		FabricClk_I 		: in STD_LOGIC;	-- GBT fabric clk
		GBT_RxFrameClk_O	: out STD_LOGIC; --Rx GBT frame clk 40MHz
		
		Board_data_I		: in board_data_type; --PM or TCM data
		Control_register_I	: in CONTROL_REGISTER_type;
		
		MGT_RX_P_I 		: in  STD_LOGIC;
		MGT_RX_N_I 		: in  STD_LOGIC;
		MGT_TX_P_O 		: out  STD_LOGIC;
		MGT_TX_N_O		: out  STD_LOGIC;
		MGT_TX_dsbl_O 	: out  STD_LOGIC;
		
		-- GBT data to/from FIT readout 
		RxData_rxclk_to_FITrd_I 	: in  std_logic_vector(GBT_data_word_bitdepth-1 downto 0);
		IsRxData_rxclk_to_FITrd_I	: in  STD_LOGIC;
		Data_from_FITrd_O 			: out  std_logic_vector(GBT_data_word_bitdepth-1 downto 0);
		IsData_from_FITrd_O			: out  STD_LOGIC;
		
		-- GBT data to/from GBT project
		Data_to_GBT_I 	: in  std_logic_vector(GBT_data_word_bitdepth-1 downto 0);
		IsData_to_GBT_I	: in  STD_LOGIC;
		RxData_rxclk_from_GBT_O 	: out  std_logic_vector(GBT_data_word_bitdepth-1 downto 0);
		IsRxData_rxclk_from_GBT_O	: out  STD_LOGIC;

		-- FIT readour status, including BCOR_ID to PM/TCM
		FIT_GBT_status_O : out FIT_GBT_status_type
		
	);
end FIT_GBT_project;

architecture Behavioral of FIT_GBT_project is
	attribute keep : string;	

-- reset signals
signal FSM_Clocks 				: FSM_Clocks_type;
signal reset_to_syscount 		: std_logic;
signal Is_SysClkCounter_ready   : std_logic;

-- from rx sync
signal RX_IsData_DataClk		:  std_logic; 
attribute keep of RX_IsData_DataClk : signal is "true";
signal RX_exData_from_RXsync    :  std_logic_vector(GBT_data_word_bitdepth+GBT_slowcntr_bitdepth-1 downto 0);
attribute keep of RX_exData_from_RXsync : signal is "true";

signal RX_Data_DataClk 			:  std_logic_vector(GBT_data_word_bitdepth-1 downto 0); 
signal RX_Phase_Counter 		:  std_logic_vector(rx_phase_bitdepth-1 downto 0);

-- status
signal from_gbt_bank_prj_GBT_status : Type_GBT_status;
signal FIT_GBT_STATUS 				: FIT_GBT_status_type;

-- from data generator
signal Board_data_from_main_gen		: board_data_type;
attribute keep of Board_data_from_main_gen : signal is "true";

signal RX_IsData_from_datagen 		:  std_logic; 
attribute keep of RX_IsData_from_datagen : signal is "true";
signal RX_Data_from_datagen 		:  std_logic_vector(GBT_data_word_bitdepth-1 downto 0); 
attribute keep of RX_Data_from_datagen : signal is "true";

-- from rx data decoder
signal ORBC_ID_from_RXdecoder 			: std_logic_vector(Orbit_id_bitdepth + BC_id_bitdepth-1 downto 0); -- EVENT ID from CRUS
signal ORBC_ID_corrected_from_RXdecoder : std_logic_vector(Orbit_id_bitdepth + BC_id_bitdepth-1 downto 0); -- EVENT ID to PM/TCM
signal Trigger_from_RXdecoder 			: std_logic_vector(Trigger_bitdepth-1 downto 0);
signal Readout_Mode_from_RXdecoder 		: Type_Readout_Mode;
signal BCIDsync_Mode_from_RXdecoder 	: Type_BCIDsync_Mode;

-- from data packajer
signal RX_IsData_from_packager 		:  std_logic; 
attribute keep of RX_IsData_from_packager : signal is "true";

signal RX_Data_from_packager 		:  std_logic_vector(GBT_data_word_bitdepth-1 downto 0); 
attribute keep of RX_Data_from_packager : signal is "true";

-- from GBT Rx
signal RX_IsData_rxclk_from_GBT		:  std_logic; 
attribute keep of RX_IsData_rxclk_from_GBT : signal is "true";

signal RX_Data_rxclk_from_GBT 		:  std_logic_vector(GBT_data_word_bitdepth-1 downto 0); 
attribute keep of RX_Data_rxclk_from_GBT : signal is "true";

begin
-- WIRING ======================================================
	FSM_Clocks.System_Clk 	<= SysClk_I;
	FSM_Clocks.Data_Clk 	<= DataClk_I;

	-- SFP turned ON
	MGT_TX_dsbl_O <= '0';

	-- Status
	FIT_GBT_status_O 							<= FIT_GBT_STATUS;
	FIT_GBT_STATUS.BCID_from_CRU 				<= ORBC_ID_from_RXdecoder(BC_id_bitdepth-1 downto 0);
	FIT_GBT_STATUS.ORBIT_from_CRU 				<= ORBC_ID_from_RXdecoder(Orbit_id_bitdepth + BC_id_bitdepth-1 downto BC_id_bitdepth);
	FIT_GBT_STATUS.BCID_from_CRU_corrected 		<= ORBC_ID_corrected_from_RXdecoder(BC_id_bitdepth-1 downto 0);
	FIT_GBT_STATUS.ORBIT_from_CRU_corrected 	<= ORBC_ID_corrected_from_RXdecoder(Orbit_id_bitdepth + BC_id_bitdepth-1 downto BC_id_bitdepth);
	FIT_GBT_STATUS.Trigger_from_CRU 			<= Trigger_from_RXdecoder;
	FIT_GBT_STATUS.Readout_Mode 				<= Readout_Mode_from_RXdecoder;
	FIT_GBT_STATUS.BCIDsync_Mode 				<= BCIDsync_Mode_from_RXdecoder;
	FIT_GBT_STATUS.rx_phase 					<= RX_Phase_Counter;
	FIT_GBT_STATUS.GBT_status 					<= from_gbt_bank_prj_GBT_status;

	RX_Data_DataClk <= RX_exData_from_RXsync(GBT_data_word_bitdepth-1 downto 0);
	
	Data_from_FITrd_O 	<= RX_Data_from_packager;
	IsData_from_FITrd_O	<= RX_IsData_from_packager;
	
	RxData_rxclk_from_GBT_O <= RX_Data_rxclk_from_GBT;
	IsRxData_rxclk_from_GBT_O <= RX_IsData_rxclk_from_GBT;

-- =============================================================

-- Reset FSM =================================================
Reset_Generator_comp: entity work.Reset_Generator
port map(
			RESET_I => RESET_I,
			SysClk_I => SysClk_I,
			DataClk_I => DataClk_I,
			Sys_Cntr_ready_I => Is_SysClkCounter_ready,
			Reset_DClk_O => reset_to_syscount,
			General_reset_O => FSM_Clocks.Reset
		);
-- ===========================================================

-- Data Clk strobe ===========================================
DataClk_I_strobe_comp: entity work.DataClk_strobe
port map(
			RESET_I => reset_to_syscount,
			SysClk_I => SysClk_I,
			DataClk_I => DataClk_I,
			SysClk_count_O => FSM_Clocks.System_Counter,
			Counter_ready_O => Is_SysClkCounter_ready
		);
-- ===========================================================

-- RX Data Clk Sync ============================================
RxData_ClkSync_comp : entity work.RXDATA_CLKSync
port map (
			FSM_Clocks_I => FSM_Clocks,
			RX_CLK_I  => RxDataClk_I,

			RX_IS_DATA_RXCLK_I   => IsRxData_rxclk_to_FITrd_I,
			RX_DATA_RXCLK_I      => x"0"&RxData_rxclk_to_FITrd_I,
			RX_IS_DATA_DATACLK_O => RX_IsData_DataClk,
			RX_DATA_DataClk_O    => RX_exData_from_RXsync,
			CLK_PH_CNT_O         => RX_Phase_Counter
);
-- =============================================================

-- RX Data Decoder ============================================
RX_Data_Decoder_comp : entity work.RX_Data_Decoder
Port map ( 
		FSM_Clocks_I => FSM_Clocks,
		
		FIT_GBT_status_I => FIT_GBT_STATUS,
		Control_register_I => Control_register_I,
			
		RX_IsData_I => RX_IsData_from_datagen,
		RX_Data_I => RX_Data_from_datagen,
		
		ORBC_ID_from_CRU_O => ORBC_ID_from_RXdecoder,
		ORBC_ID_from_CRU_corrected_O => ORBC_ID_corrected_from_RXdecoder,
		Trigger_O => Trigger_from_RXdecoder,
		
		Readout_Mode_O => Readout_Mode_from_RXdecoder,
		BCIDsync_Mode_O => BCIDsync_Mode_from_RXdecoder
	 );
-- =============================================================

-- Test_Generator ===============================================
MAIN_Data_Gen_comp : entity work.MAIN_Data_Gen
port map (
		FSM_Clocks_I => FSM_Clocks,
		
		FIT_GBT_status_I => FIT_GBT_STATUS,
		Control_register_I => Control_register_I,
		
		Board_data_I => Board_data_I,
		
		RX_IsData_I => RX_IsData_DataClk,
		RX_Data_I => RX_Data_DataClk,
		
		Board_data_O => Board_data_from_main_gen,
		RX_IsData_O => RX_IsData_from_datagen,
		RX_Data_O => RX_Data_from_datagen
);
-- =============================================================


-- Data Packager ===============================================
Data_Packager_comp : entity work.Data_Packager
port map (
		FSM_Clocks_I 		=> FSM_Clocks,
		
		FIT_GBT_status_I 	=> FIT_GBT_STATUS,
		Control_register_I 	=> Control_register_I,
		
		Board_data_I => Board_data_from_main_gen,
		
		TX_Data_O 			=> RX_Data_from_packager,
		TX_IsData_O 		=> RX_IsData_from_packager
);
-- =============================================================

-- GBT BANK Designe ===========================================
     gbtBankDsgn_gen: if GENERATE_GBT_BANK = ENABLED generate
        
		 gbtBankDsgn : entity work.GBT_bank_designe
		 port map (
			RESET_I  => RESET_I,
			FabricClk_I => DataClk_I,
			MgtRefClk_I => MgtRefClk_I,
			TXDataClk_I => DataClk_I,
			
			SFP_RX_P_I => MGT_RX_P_I,
			SFP_RX_N_I => MGT_RX_N_I,
			SFP_TX_P_O => MGT_TX_P_O,
			SFP_TX_N_O => MGT_TX_N_O,
			
			TXData_I => Data_to_GBT_I,
			IsTXData_I => IsData_to_GBT_I,
			
			RXDataClk_O => GBT_RxFrameClk_O,
			RXData_O => RX_Data_rxclk_from_GBT,
			IsRXData_O => RX_IsData_rxclk_from_GBT,
			
			GBT_Status_O => from_gbt_bank_prj_GBT_status
		);
				 
	end generate;

			
     gbtBankDsgn_void_gen: if GENERATE_GBT_BANK = DISABLED generate
        
			MGT_TX_P_O <= '0';
			MGT_TX_N_O <= '0';
			
			GBT_RxFrameClk_O <= '0';
			RxData_rxclk_from_GBT_O <= (others => '0');
			IsRxData_rxclk_from_GBT_O <= '0';
			
			from_gbt_bank_prj_GBT_status.txFrameClk_from_gbtExmplDsgn <= '0';
			from_gbt_bank_prj_GBT_status.txWordClk_from_gbtExmplDsgn <= '0';
			from_gbt_bank_prj_GBT_status.rxFrameClk_from_gbtExmplDsgn <= '0';
			from_gbt_bank_prj_GBT_status.rxWordClk_from_gbtExmplDsgn <= '0';
			from_gbt_bank_prj_GBT_status.txOutClkFabric_from_gbtExmplDsgn <= '0';
			from_gbt_bank_prj_GBT_status.mgt_cplllock_from_gbtExmplDsgn <= '0';
			from_gbt_bank_prj_GBT_status.rxWordClkReady_from_gbtExmplDsgn <= '0';
			from_gbt_bank_prj_GBT_status.rxFrameClkReady_from_gbtExmplDsgn <= '0';
			from_gbt_bank_prj_GBT_status.mgtLinkReady_from_gbtExmplDsgn <= '0';
			from_gbt_bank_prj_GBT_status.tx_resetDone_from_gbtExmplDsgn <= '0';
			from_gbt_bank_prj_GBT_status.tx_fsmResetDone_from_gbtExmplDsgn <= '0';
			from_gbt_bank_prj_GBT_status.gbtRx_Ready_from_gbt_ExmplDsgn <= '0';
			from_gbt_bank_prj_GBT_status.gbtTx_Ready_from_gbt_ExmplDsgn <= '0';

			
	end generate;

 -- =============================================================

end Behavioral;

