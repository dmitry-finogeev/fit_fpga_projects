#####################################################################
########################## FIT GBT Project ##########################
#####################################################################


# Clocks
#created in kc705 constrains
#create_clock -period 25.000 -name FMC_HPC_clk_A -waveform {0.000 12.500} [get_ports FMC_HPC_clk_A_p]
#create_clock -period 5.000 -name FMC_HPC_clk_200 -waveform {0.000 2.500} [get_nets FMC_HPC_clk_200_p]
create_clock -period 5.000 -name USER_CLK_P -waveform {0.000 2.500} [get_ports USER_CLK_P]

create_clock -period 8.333 -name RxWordCLK [get_pins {FitGbtPrg/gbtBankDsgn_gen.gbtBankDsgn/gbtExmplDsgn_inst/gbtBank/mgt_param_package_src_gen.mgt/mgtLatOpt_gen.mgtLatOpt/gtxLatOpt_gen[1].xlx_k7v7_mgt_std_i/U0/xlx_k7v7_mgt_ip_i/gt0_xlx_k7v7_mgt_ip_i/gtxe2_i/RXOUTCLK}]
create_clock -period 8.333 -name TxWordCLK [get_pins {FitGbtPrg/gbtBankDsgn_gen.gbtBankDsgn/gbtExmplDsgn_inst/gbtBank/mgt_param_package_src_gen.mgt/mgtLatOpt_gen.mgtLatOpt/gtxLatOpt_gen[1].xlx_k7v7_mgt_std_i/U0/xlx_k7v7_mgt_ip_i/gt0_xlx_k7v7_mgt_ip_i/gtxe2_i/TXOUTCLK}]
#not used
#create_generated_clock -name SystemCLK [get_pins CDMClkpllcomp/inst/plle2_adv_inst/CLKOUT1]
create_generated_clock -name RXDataCLK [get_pins {FitGbtPrg/*/gbtExmplDsgn_inst/gbtBank_Clk_gen[1].gbtBank_rxFrmClkPhAlgnr/latOpt_phalgnr_gen.mmcm_inst/pll/inst/mmcm_adv_inst/CLKOUT0}]




#clock groups for standalone progect
#for integrated use spetial groups
######################################
set_clock_groups -name ASYNC_CLOCKS -asynchronous \
-group [get_clocks -include_generated_clocks {RxWordCLK RXDataCLK}] \
-group [get_clocks -include_generated_clocks TxWordCLK] \
-group [get_clocks -include_generated_clocks eth_refclk] \
-group [get_clocks -include_generated_clocks ipbus_module/infra/eth/phy/*/gtxe2_i/TXOUTCLK] \
-group [get_clocks -include_generated_clocks ipbus_module/infra/eth/phy/*/gtxe2_i/RXOUTCLK]

#not used
#-group [get_clocks -include_generated_clocks {FMC_HPC_clk_200 FMC_HPC_clk_A}] \


######################################



# RX DATA CLK synk latches
set_property ASYNC_REG true [get_cells FitGbtPrg/RxData_ClkSync_comp/RX_CLK_from_ff00_reg]
set_property ASYNC_REG true [get_cells FitGbtPrg/RxData_ClkSync_comp/RX_CLK_from_ff01_reg]


# Reset generator multipath
#set_multicycle_path -setup -start -reset_path -from [get_cells FitGbtPrg/Reset_Generator_comp/General_reset_ff_reg*] -to [get_cells FitGbtPrg/Reset_Generator_comp/GenRes_DataClk_ff_reg*] 8
#set_multicycle_path -setup -start -reset_path -from [get_cells FitGbtPrg/Reset_Generator_comp/Cntr_reset_ff_reg*] -to [get_cells FitGbtPrg/DataClk_I_strobe_comp/count_ready_dtclk_ff_reg*] 8
#set_multicycle_path -hold -reset_path -from [get_cells FitGbtPrg/Reset_Generator_comp/General_reset_ff_reg*] -to [get_cells FitGbtPrg/Reset_Generator_comp/GenRes_DataClk_ff_reg*] 7
#set_multicycle_path -hold -reset_path -from [get_cells FitGbtPrg/Reset_Generator_comp/Cntr_reset_ff_reg*] -to [get_cells FitGbtPrg/DataClk_I_strobe_comp/count_ready_dtclk_ff_reg*] 7
#set_multicycle_path -setup -reset_path -from [get_cells FitGbtPrg/Reset_Generator_comp/GenRes_DataClk_ff_reg*] -to [get_cells FitGbtPrg/RxData_ClkSync_comp/*] 8
#set_multicycle_path -hold -end -reset_path -from [get_cells FitGbtPrg/Reset_Generator_comp/GenRes_DataClk_ff_reg*] -to [get_cells FitGbtPrg/RxData_ClkSync_comp/*] 7
#set_multicycle_path -setup -reset_path -from [get_cells FitGbtPrg/Reset_Generator_comp/GenRes_DataClk_ff_reg*] -to [get_cells FitGbtPrg/Data_Packager_comp/DataConverter_TCM_gen.DataConverter_TCM_comp/*] 8
#set_multicycle_path -hold -end -reset_path -from [get_cells FitGbtPrg/Reset_Generator_comp/GenRes_DataClk_ff_reg*] -to [get_cells FitGbtPrg/Data_Packager_comp/DataConverter_TCM_gen.DataConverter_TCM_comp/*] 7

set_false_path -from [get_cells FitGbtPrg/Reset_Generator_comp/General_reset_ff_reg]
set_false_path -from [get_cells FitGbtPrg/gbtBankDsgn_gen.gbtBankDsgn/gbtExmplDsgn_inst/gbtBank_rst_gen[1].gbtBank_gbtBankRst/gbtResetRx_from_generalRstFsm_reg]
set_false_path -from [get_cells FitGbtPrg/gbtBankDsgn_gen.gbtBankDsgn/gbtExmplDsgn_inst/gbtBank_rst_gen[1].gbtBank_gbtBankRst/mgtResetRx_from_generalRstFsm_reg]
set_false_path -from [get_cells FitGbtPrg/gbtBankDsgn_gen.gbtBankDsgn/gbtExmplDsgn_inst/gbtBank_rst_gen[1].gbtBank_gbtBankRst/gbtResetTx_from_generalRstFsm_reg]
                                
#IP BUS control register
#set_false_path -from [get_cells fit_ipbus_control/ipbus_status_reg_map_dc_reg[*][*]] -to [get_cells fit_ipbus_control/ipbus_status_reg_ipbclk_reg[*][*]]
#set_false_path -from [get_cells fit_ipbus_control/Control_register_reg_dc*] -to [get_cells fit_ipbus_control/ipbus_control_reg_reg[*][*]]

set_max_delay -datapath_only -from [get_cells fit_ipbus_control/ipbus_status_reg_map_dc_reg[*][*]] -to [get_cells fit_ipbus_control/ipbus_status_reg_ipbclk_reg[*][*]] 10.000
set_max_delay -datapath_only -from [get_cells fit_ipbus_control/ipbus_control_reg_reg[*][*]] -to [get_cells fit_ipbus_control/Control_register_reg_dc*] 10.000
set_max_delay -datapath_only -from [get_cells ipbus_module/infra/clocks/rst_ipb_reg] -to [get_cells fit_ipbus_control/ipbus_status_reg_map_dc_reg[*][*]] 10.000
set_max_delay -datapath_only -from [get_cells ipbus_module/infra/clocks/rst_ipb_reg] -to [get_cells fit_ipbus_control/Control_register_reg_dc_reg*] 10.000

fit_ipbus_control/Control_register_reg_dc_reg[Data_Gen][n_cycle_void][13]/R
# RX Sync comp
set_max_delay -datapath_only -from [get_clocks RXDataCLK] -to [get_cells {FitGbtPrg/RxData_ClkSync_comp/RX_DATA_DATACLK_ffsc_reg[*]}] 3.000
set_max_delay -datapath_only -from [get_clocks RXDataCLK] -to [get_cells FitGbtPrg/RxData_ClkSync_comp/RX_CLK_from_ff00_reg] 1.000


# Clock strobe
#set_multicycle_path -setup -start -from [get_pins {FitGbtPrg/h/CLK_PH_counter_stop_ff_sc_reg[*]/C}] -to [get_pins {FitGbtPrg/RxData_ClkSync_comp/CLK_PH_counter_ff_dc_reg[*]/D}] 8
#set_multicycle_path -setup -start -from [get_cells FitGbtPrg/DataClk_I_strobe_comp/count_ready_reg*] -to [get_cells FitGbtPrg/DataClk_I_strobe_comp/count_ready_dtclk_ff_reg*] 8

#set_multicycle_path -hold -from [get_pins {FitGbtPrg/RxData_ClkSync_comp/CLK_PH_counter_stop_ff_sc_reg[*]/C}] -to [get_pins {FitGbtPrg/RxData_ClkSync_comp/CLK_PH_counter_ff_dc_reg[*]/D}] 7
#set_multicycle_path -hold -from [get_cells FitGbtPrg/DataClk_I_strobe_comp/count_ready_reg*] -to [get_cells FitGbtPrg/DataClk_I_strobe_comp/count_ready_dtclk_ff_reg*] 7

#set_multicycle_path -setup -from [get_clocks SystemCLK] -to [get_cells FitGbtPrg/DataClk_I_strobe_comp/DataClk_ff_sysclk_reg*] 8
#set_multicycle_path -hold -end -from [get_clocks SystemCLK] -to [get_cells FitGbtPrg/DataClk_I_strobe_comp/DataClk_ff_sysclk_reg*] 7

#set_multicycle_path -setup -from [get_clocks SystemCLK] -to [get_cells FitGbtPrg/DataClk_I_strobe_comp/count_reset_reg*] 8
#set_multicycle_path -hold -end -from [get_clocks SystemCLK] -to [get_cells FitGbtPrg/DataClk_I_strobe_comp/count_reset_reg*] 7

#set_multicycle_path -setup -from [get_cells FitGbtPrg/DataClk_I_strobe_comp/count_ready_dtclk_ff_reg*] -to [get_cells FitGbtPrg/Reset_Generator_comp/General_reset_ff_reg*] 8
#set_multicycle_path -hold -end -from [get_cells FitGbtPrg/DataClk_I_strobe_comp/count_ready_dtclk_ff_reg*] -to [get_cells FitGbtPrg/Reset_Generator_comp/General_reset_ff_reg*] 7


# Data Converter TCM
set_multicycle_path -setup -from [get_cells FitGbtPrg/Data_Packager_comp/DataConverter_TCM_gen.DataConverter_TCM_comp/Board_data_dataclkff_reg*] -to [get_cells FitGbtPrg/Data_Packager_comp/DataConverter_TCM_gen.DataConverter_TCM_comp/Board_data_sysclkff_reg*] 8
set_multicycle_path -hold -end -from [get_cells FitGbtPrg/Data_Packager_comp/DataConverter_TCM_gen.DataConverter_TCM_comp/Board_data_dataclkff_reg*] -to [get_cells FitGbtPrg/Data_Packager_comp/DataConverter_TCM_gen.DataConverter_TCM_comp/Board_data_sysclkff_reg*] 7







##===================================================================================================##
##======================================  FLOORPLANNING  ============================================##
##===================================================================================================##

##==========##
## P BLOCKS ##
##==========##

#create_pblock sfpQuad_area
#add_cells_to_pblock [get_pblocks sfpQuad_area] [get_cells -quiet [list genRst]]
#resize_pblock [get_pblocks sfpQuad_area] -add {CLOCKREGION_X1Y5:CLOCKREGION_X1Y5}

##============##
## PRIMITIVES ##
##============##

## TX_FRAMECLK PLL:
##-----------------

#used in exmpl ds; generate frame clk
#set_property LOC PLLE2_ADV_X0Y6 [get_cells txPll/inst/plle2_adv_inst]

## RX_FRAMECLK PHASE ALIGNER (Lat Opt only):
##------------------------------------------

set_property LOC MMCME2_ADV_X0Y6 [get_cells FitGbtPrg/*/gbtExmplDsgn_inst/*/latOpt_phalgnr_gen.mmcm_inst/pll/inst/mmcm_adv_inst]
set_property CLOCK_DEDICATED_ROUTE FALSE [get_nets FitGbtPrg/*/gbtExmplDsgn_inst/*/latOpt_phalgnr_gen.mmcm_inst/pll/inst/clk_out1_xlx_k7v7_gbt_rx_frameclk_phalgnr_mmcm]

##===================================================================================================##
##===================================================================================================##

##===================================================================================================##
##====================================  TIMING CLOSURE  =============================================##
##===================================================================================================##

##=================================================##
## Latency-Optimized (LATOPT) specific constraints ##
##=================================================##


##!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!##
##!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!##
##                                                                           ##
## Comment: Note!! Uncomment out the following constraints when implementing ##
##                 the Latency-Optimized (LATOPT) version.                   ##
##                                                                           ##
##!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!##
##!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!##

## GBT TX:
##--------

## RX Phase Aligner
##-----------------
## Comment: The period of TX_FRAMECLK is 25ns but "TS_GBTTX_SCRAMBLER_TO_GEARBOX" is set to 16ns, providing 9ns for setup margin.
set_false_path -hold -from [get_clocks RXDataCLK] -to [get_pins {FitGbtPrg/*/gbtExmplDsgn_inst/gbtBank_Clk_gen[1].gbtBank_rxFrmClkPhAlgnr/latOpt_phalgnr_gen.phase_computing_inst/serialToParallel_reg[0]/D}]
set_max_delay -from [get_clocks RXDataCLK] -to [get_pins {FitGbtPrg/*/gbtExmplDsgn_inst/gbtBank_Clk_gen[1].gbtBank_rxFrmClkPhAlgnr/latOpt_phalgnr_gen.phase_computing_inst/serialToParallel_reg[0]/D}] 1.000


## GBT RX:
##--------

## Comment: The period of RX_FRAMECLK is 25ns but "TS_GBTRX_GEARBOX_TO_DESCRAMBLER" is set to 20ns, providing 5ns for setup margin.
set_max_delay -datapath_only -from [get_pins -hier -filter {NAME =~ */*/*/scrambler/*/C}] -to [get_pins -hier -filter {NAME =~ */*/*/txGearbox/*/D}] 16.000
set_max_delay -datapath_only -from [get_pins -hier -filter {NAME =~ */*/*/rxGearbox/*/C}] -to [get_pins -hier -filter {NAME =~ */*/*/descrambler/*/D}] 20.000
set_max_delay -datapath_only -from [get_pins -hier -filter {NAME =~ */*/*/rxGearbox/*/C}] -to [get_pins -hier -filter {NAME =~ */*/*/descrambler/*/D}] 20.000
set_max_delay -datapath_only -from [get_pins -hier -filter {NAME =~ */*/*/rxGearbox/*/C}] -to [get_pins -hier -filter {NAME =~ */*/*/descrambler/*/D}] 20.000
set_max_delay -datapath_only -from [get_pins -hier -filter {NAME =~ */*/*/rxGearbox/*/C}] -to [get_pins -hier -filter {NAME =~ */*/*/descrambler/*/D}] 20.000

##===================================================================================================##
##===================================================================================================##



