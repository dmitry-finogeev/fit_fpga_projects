----------------------------------------------------------------------------------
-- Company: INR RAS
-- Engineer: Finogeev D. A. dmitry-finogeev@yandex.ru
-- 
-- Create Date:    07/11/2017 
-- Design Name: 
-- Module Name:   
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision
-- Additional Comments: 
--
----------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use ieee.numeric_std.all;
use ieee.std_logic_unsigned.all ;

use work.fit_gbt_common_package.all;
use work.fit_gbt_board_package.all;


entity DataConverter_TCM is
    Port ( 
		FSM_Clocks_I : in FSM_Clocks_type;
		
		FIT_GBT_status_I : in FIT_GBT_status_type;
		Control_register_I : in CONTROL_REGISTER_type;
		
		Board_data_I		: in board_data_type;
		
		FIFO_is_space_for_packet_I : in STD_LOGIC;
		
		FIFO_WE_O : out STD_LOGIC;
		FIFO_data_word_O : out std_logic_vector(fifo_data_bitdepth-1 downto 0)
	 );
end DataConverter_TCM;

architecture Behavioral of DataConverter_TCM is

	constant data_words_to_send	: std_logic_vector(tdwords_bitdepth-1 downto 0) := "01000"; 
	constant n_pck_wrds_void 	: std_logic_vector(n_pckt_wrds_bitdepth-tdwords_bitdepth-1 downto 0) := (others => '0');
	
	signal Board_data_void 												:  Board_data_type;
	signal Board_data_dataclkff, Board_data_dataclkff_next 				:  Board_data_type;
	signal Board_data_sysclkff, Board_data_sysclkff_next 				:  Board_data_type;
	signal FIFO_is_space_for_packet_ff, FIFO_is_space_for_packet_ff_next:  std_logic;
	
	signal Is_sending_state_ff, Is_sending_state_ff_next	: std_logic;
	signal word_counter_ff, word_counter_ff_next :  std_logic_vector(tdwords_bitdepth-1 downto 0);
	
	signal header_word 		: std_logic_vector(fifo_data_bitdepth-1 downto 0);

	signal FIFO_WE_ff, FIFO_WE_ff_next : STD_LOGIC;
	signal FIFO_data_word_ff, FIFO_data_word_ff_next : std_logic_vector(fifo_data_bitdepth-1 downto 0);

begin

-- Wiring ********************************************
	FIFO_WE_O <= FIFO_WE_ff;
	FIFO_data_word_O <= FIFO_data_word_ff;
-- ***************************************************

-- Header format *************************************
--	header_word <= n_pck_wrds_void & data_words_to_send & Board_data_sysclkff.rx_phase & Board_data_sysclkff.Trigger_ID(23 downto 0) & Board_data_sysclkff.ORBC_ID;
	header_word <= n_pck_wrds_void & data_words_to_send & Board_data_sysclkff.rx_phase & x"00_0000" & Board_data_sysclkff.ORBC_ID;
-- ***************************************************


-- Data constants generation *************************
    Board_data_gen_f: for n_word in 0 to total_data_words-1 generate
		Board_data_void.data_array(n_word) <= (others => '0');
    end generate;
	Board_data_void.is_data <= '0';
	Board_data_void.rx_phase <= (others => '0');
	Board_data_void.ORBC_ID <= (others => '0');
-- ****************************************************

-- Data ff data clk ***********************************
	PROCESS (FSM_Clocks_I.Data_Clk)
	BEGIN
		IF(FSM_Clocks_I.Data_Clk'EVENT and FSM_Clocks_I.Data_Clk = '1') THEN
			IF(FSM_Clocks_I.Reset = '1') THEN
				Board_data_dataclkff <= Board_data_void;
			ELSE
				Board_data_dataclkff <= Board_data_dataclkff_next;
			END IF;
		END IF;
	END PROCESS;
-- ****************************************************

-- Data ff sys clk ************************************
	PROCESS (FSM_Clocks_I.System_Clk)
	BEGIN
		IF(FSM_Clocks_I.System_Clk'EVENT and FSM_Clocks_I.System_Clk = '1') THEN
			IF(FSM_Clocks_I.Reset = '1') THEN
				Board_data_sysclkff <= Board_data_void;
				Is_sending_state_ff <= '0';
				word_counter_ff <= (others => '0');
				FIFO_is_space_for_packet_ff <= '0';
				
				FIFO_WE_ff <= '0';
				FIFO_data_word_ff <= (others => '0');
			ELSE
				word_counter_ff <= word_counter_ff_next;
				Is_sending_state_ff <= Is_sending_state_ff_next;
				Board_data_sysclkff <= Board_data_sysclkff_next;
				FIFO_is_space_for_packet_ff <= FIFO_is_space_for_packet_ff_next;
				
				FIFO_WE_ff <= FIFO_WE_ff_next;
				FIFO_data_word_ff <= FIFO_data_word_ff_next;
			END IF;
		END IF;
	END PROCESS;
-- ****************************************************


-- FSM ***********************************************
Board_data_dataclkff_next <= Board_data_I;
	
Board_data_sysclkff_next <= Board_data_void      WHEN (FSM_Clocks_I.Reset = '1') ELSE
							Board_data_dataclkff WHEN (FSM_Clocks_I.System_Counter = x"7") and (Is_sending_state_ff_next = '0') ELSE
							Board_data_sysclkff;

							
FIFO_is_space_for_packet_ff_next <= '0'							WHEN (FSM_Clocks_I.Reset = '1') ELSE
									FIFO_is_space_for_packet_I	WHEN (FSM_Clocks_I.System_Counter = x"7") and (Is_sending_state_ff_next = '0') ELSE
									FIFO_is_space_for_packet_ff;
									
									
Is_sending_state_ff_next <= '0'					WHEN (FSM_Clocks_I.Reset = '1') ELSE
							'0'					WHEN (word_counter_ff = data_words_to_send) ELSE
							'1'					WHEN (FSM_Clocks_I.System_Counter = x"0") and (Board_data_sysclkff.is_data = '1') and (Is_sending_state_ff = '0') ELSE
							Is_sending_state_ff;
		
		
word_counter_ff_next <= 	(others => '0') 	WHEN (FSM_Clocks_I.Reset = '1') ELSE

							(others => '0')		WHEN (Is_sending_state_ff = '0') ELSE
							(others => '0')		WHEN (word_counter_ff = data_words_to_send) ELSE
							word_counter_ff + 1;
							
							
FIFO_data_word_ff_next <= 	(others => '0') 	WHEN (FSM_Clocks_I.Reset = '1') ELSE
							header_word 		WHEN (FSM_Clocks_I.System_Counter = x"0") and (Board_data_sysclkff.is_data = '1') and (Is_sending_state_ff = '0') ELSE
							Board_data_sysclkff.data_array( to_integer(unsigned(word_counter_ff)) ) WHEN (Is_sending_state_ff = '1') ELSE
							(others => '0');
							
FIFO_WE_ff_next		 <= 	'0'				 	WHEN (FSM_Clocks_I.Reset = '1') ELSE
							'1' 				WHEN (FSM_Clocks_I.System_Counter = x"0") and (Board_data_sysclkff.is_data = '1') and (FIFO_is_space_for_packet_ff = '1') ELSE
							'1' 				WHEN (Is_sending_state_ff = '1') and (FIFO_is_space_for_packet_ff = '1') ELSE
							'0';
-- ****************************************************
							

end Behavioral;

