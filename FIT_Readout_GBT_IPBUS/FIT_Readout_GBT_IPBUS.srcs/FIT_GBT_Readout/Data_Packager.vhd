----------------------------------------------------------------------------------
-- Company: INR RAS
-- Engineer: Finogeev D.A. dmitry-finogeev@yandex.ru
-- 
-- Create Date:    12:03:02 01/09/2017 
-- Design Name: 	FIT GBT
-- Module Name:    Data_Packager - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use work.fit_gbt_common_package.all;
use work.fit_gbt_board_package.all;

use ieee.numeric_std.all;


entity Data_Packager is
    Port ( 
		FSM_Clocks_I : in FSM_Clocks_type;

		FIT_GBT_status_I : in FIT_GBT_status_type;
		Control_register_I : in CONTROL_REGISTER_type;  
		
		Board_data_I		: in board_data_type;
		
		TX_Data_O : out std_logic_vector(GBT_data_word_bitdepth-1 downto 0);
		TX_IsData_O : out STD_LOGIC
	 );
end Data_Packager;

architecture Behavioral of Data_Packager is

	signal FIFO_WE_from_converter : std_logic;
	signal FIFO_data_word_from_from_converter : std_logic_vector(fifo_data_bitdepth-1 downto 0);

	
	signal data_from_cru_constructor : std_logic_vector(GBT_data_word_bitdepth-1 downto 0);
	signal is_data_from_cru_constructor : STD_LOGIC;
	
	signal raw_data_fifo_words_count : std_logic_vector(fifo_count_bitdepth-1 downto 0);
	signal raw_data_fifo_data_out : std_logic_vector(fifo_data_bitdepth-1 downto 0);
	signal raw_data_fifo_isempty : std_logic;
	signal raw_data_fifo_rden : std_logic;
	signal raw_data_fifo_space_is_for_packet : STD_LOGIC;
	
	signal selected_data_out : std_logic_vector(fifo_data_bitdepth-1 downto 0);
	signal selected_data_isempty : boolean;
	signal selected_data_rden : std_logic;
	
	signal fifo_reset : std_logic;


begin
-- --Wiring =====================================================
-- TX_Data_O(83 downto 0) <= (others=>'0'); -- test generation
raw_data_fifo_space_is_for_packet <= 	'1' when (unsigned(raw_data_fifo_words_count) <= fifo_depth-total_data_words-1) else
										'0';
-- -- ===========================================================


-- Data Converter ===============================================
-- now only TCM ready for tests
DataConverter_TCM_gen: if (Board_DataConversion_type = one_word) or (Board_DataConversion_type = one_word) generate

DataConverter_TCM_comp: entity work.DataConverter_TCM
    port map(
		FSM_Clocks_I => FSM_Clocks_I,

		FIT_GBT_status_I => FIT_GBT_status_I,
		Control_register_I => Control_register_I,
		
		Board_data_I => Board_data_I,
		
		FIFO_is_space_for_packet_I => raw_data_fifo_space_is_for_packet,
		
		FIFO_WE_O => FIFO_WE_from_converter,
		FIFO_data_word_O => FIFO_data_word_from_from_converter
		);
		
end generate;

-- ===========================================================


-- Raw_data_fifo =============================================
raw_data_fifo_comp : entity work.raw_data_fifo
port map(
           wr_clk         => FSM_Clocks_I.System_Clk,
           rd_clk         => FSM_Clocks_I.Data_Clk,
     	   wr_data_count  => raw_data_fifo_words_count,
--           SRST        => fifo_reset,
           rst        => FSM_Clocks_I.Reset,
           WR_EN 		=> FIFO_WE_from_converter,
--           RD_EN       => raw_data_fifo_rden,
           RD_EN       => selected_data_rden,
           DIN         => FIFO_data_word_from_from_converter,
           DOUT        => raw_data_fifo_data_out,
           FULL        => open,
           EMPTY       => raw_data_fifo_isempty
        );
		
-- ENTITY raw_data_fifo IS
  -- PORT (
    -- rst : IN STD_LOGIC;
    -- wr_clk : IN STD_LOGIC;
    -- rd_clk : IN STD_LOGIC;
    -- din : IN STD_LOGIC_VECTOR(79 DOWNTO 0);
    -- wr_en : IN STD_LOGIC;
    -- rd_en : IN STD_LOGIC;
    -- dout : OUT STD_LOGIC_VECTOR(79 DOWNTO 0);
    -- full : OUT STD_LOGIC;
    -- almost_full : OUT STD_LOGIC;
    -- empty : OUT STD_LOGIC;
    -- wr_data_count : OUT STD_LOGIC_VECTOR(6 DOWNTO 0)
  -- );
-- END raw_data_fifo;

-- ===========================================================

-- -- Event Selector ======================================
-- Event_Selector_comp : entity work.Event_Selector
-- port map	(
			-- FSM_Clocks_I => FSM_Clocks_I,
			
			-- FIT_GBT_status_I => FIT_GBT_status_I,
			-- Control_register_I => Control_register_I,
		
			-- FIFO_data_word_I => raw_data_fifo_data_out,
			-- FIFO_Is_Empty_I => (raw_data_fifo_isempty = '1'),
			-- DATA_RE_I => selected_data_rden,
			
			-- DATA_word_O => selected_data_out,
			-- DATA_Is_Empty_O => selected_data_isempty,
			-- FIFO_RE_O => raw_data_fifo_rden,
			-- FIFO_RESET_O => fifo_reset
			-- );
-- -- ===========================================================

-- CRU Packet Constructer ======================================
CRU_packet_Builder_comp : entity work.CRU_packet_Builder
port map	(
			FSM_Clocks_I => FSM_Clocks_I,
			
			FIT_GBT_status_I => FIT_GBT_status_I,
			Control_register_I => Control_register_I,
		
			-- FIFO_data_word_I => selected_data_out,
			-- FIFO_Is_Empty_I => selected_data_isempty,
			FIFO_data_word_I => raw_data_fifo_data_out,
			FIFO_Is_Empty_I => raw_data_fifo_isempty,
			
			FIFO_RE_O => selected_data_rden,
			Is_Data_O => is_data_from_cru_constructor,
			Data_O => data_from_cru_constructor
			);
-- ===========================================================



-- TX Data Gen ===============================================
TX_Data_Gen_comp : entity work.TX_Data_Gen
port map(
			FSM_Clocks_I => FSM_Clocks_I,
			
			Control_register_I => Control_register_I,
			FIT_GBT_status_I => FIT_GBT_status_I,
			
			TX_IsData_I => is_data_from_cru_constructor,
			TX_Data_I => data_from_cru_constructor,
			
			TX_IsData_O => TX_IsData_O,
			TX_Data_O => TX_Data_O
		);
-- ===========================================================

end Behavioral;

