----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    15:42:21 04/12/2017 
-- Design Name: 
-- Module Name:    Test_Generator - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--

-- sizes, two event id, 
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use work.fit_gbt_common_package.all;
use ieee.numeric_std.all;


entity FIFO_CRU_packet_Constructor is
    Port ( 
		FSM_Clocks_I : in FSM_Clocks_type;

		FIT_GBT_status_I : in FIT_GBT_status_type;
		Control_register_I : in CONTROL_REGISTER_type;
		
		FIFO_data_word_I : in std_logic_vector(fifo_data_bitdepth-1 downto 0);
		FIFO_Is_Empty_I : in boolean;
		
		FIFO_RE_O : out STD_LOGIC;
		Is_Data_O : out STD_LOGIC;
		Data_O : out std_logic_vector(GBT_data_word_bitdepth-1 downto 0)
	 );
end FIFO_CRU_packet_Constructor;

architecture Behavioral of FIFO_CRU_packet_Constructor is

	type FSM_STATE_T is (s0_start, s1_sop, s2_data, s3_eop);
	signal FSM_STATE, FSM_STATE_NEXT  : FSM_STATE_T;
	
	constant nwords_in_SOP	: integer := 5;
	constant nwords_in_EOP	: integer := 2;
	signal Events_to_send : integer range 0 to 10;

	type SOP_format_type is array (0 to nwords_in_SOP-1) of std_logic_vector(GBT_data_word_bitdepth downto 0);
	type EOP_format_type is array (0 to nwords_in_EOP-1) of std_logic_vector(GBT_data_word_bitdepth downto 0);
    signal SOP_format : SOP_format_type;
    signal EOP_format : EOP_format_type;
	
	signal header_size : std_logic_vector(7 downto 0);
    signal HB_Orbit    : std_logic_vector(Orbit_id_bitdepth-1 downto 0);
    signal HB_BC       : std_logic_vector(BC_id_bitdepth-1 downto 0);
    signal TRG_Orbit   : std_logic_vector(Orbit_id_bitdepth-1 downto 0);
    signal TRG_BC      : std_logic_vector(BC_id_bitdepth-1 downto 0);
	signal TRG_Type    : std_logic_vector(Trigger_bitdepth-1 downto 0);

	signal SOP_Count, SOP_Count_next : integer range 0 to nwords_in_SOP;
	signal Word_Count, Word_Count_next, Words_total_latched, Words_total_latched_next : integer range 0 to 11;
	signal EOP_Count, EOP_Count_next : integer range 0 to nwords_in_EOP;
	
	signal Data_ff, Data_ff_next : std_logic_vector(GBT_data_word_bitdepth-1 downto 0);
	signal IsData_ff, IsData_ff_next : STD_LOGIC;

	signal dataheader_latched, dataheader_latched_next : std_logic_vector(GBT_data_word_bitdepth-1 downto 0); --header for first event in packets
	

begin

	header_size <= std_logic_vector(to_unsigned((nwords_in_SOP-1), 8));
	HB_Orbit    <= dataheader_latched(Orbit_id_bitdepth-1 downto 0);
	HB_BC       <= dataheader_latched(BC_id_bitdepth+Orbit_id_bitdepth-1 downto Orbit_id_bitdepth);
	TRG_Orbit   <= dataheader_latched(Orbit_id_bitdepth-1 downto 0);
	TRG_BC      <= dataheader_latched(BC_id_bitdepth+Orbit_id_bitdepth-1 downto Orbit_id_bitdepth);
	TRG_Type    <= dataheader_latched(Trigger_bitdepth+BC_id_bitdepth+Orbit_id_bitdepth-1 downto BC_id_bitdepth+Orbit_id_bitdepth);

	--             is data
	SOP_format(0) <= '0' & x"00000000000000000001"; -- SOP
	
	 
	--             is data      extra GBT            header size	Link ID	  FEE ID                      block lenght     header version
	SOP_format(1) <= '1' &      x"0000"&  x"00"&     header_size&   x"00"&    Control_register_I.FEE_ID&  x"ffff"&         x"01";
	
	
	--            is data       extra GBT
	SOP_format(2) <= '1' &      x"0000"&  HB_Orbit &  TRG_Orbit;
	
	
	--             is data      extra GBT            HB BC & HB Orbit
	SOP_format(3) <= '1' &      x"0000"&  x"00"& HB_BC & TRG_Type & TRG_BC;
	
	
	--            is data       extra GBT            PAR 	                   Detector field    stop bit     pages counter
	SOP_format(4) <= '1' &      x"0000"&  x"00"&     Control_register_I.PAR&   x"0000"&         x"01"&       x"0000";
	
	
	EOP_format(0) <= '1' & x"ffffffffffffffffffff"; -- test trailer
	
	EOP_format(1) <= '0' & x"00000000000000000002"; -- eop

	
	Is_Data_O <= IsData_ff;
	Data_O <= Data_ff;

		
	PROCESS (FSM_Clocks_I.Data_Clk)
	BEGIN
		IF(FSM_Clocks_I.Data_Clk'EVENT and FSM_Clocks_I.Data_Clk = '1') THEN
			IF(FSM_Clocks_I.Reset = '1') THEN
				IsData_ff <= '0';
				Data_ff <= (others => '0');
			ELSE
				IsData_ff <= IsData_ff_next;
				Data_ff <= Data_ff_next;
			END IF;
		END IF;
	END PROCESS;
	
	
		PROCESS (FSM_Clocks_I.System_Clk)
		BEGIN

			IF(FSM_Clocks_I.System_Clk'EVENT and FSM_Clocks_I.System_Clk = '1') THEN
			
				IF(FSM_Clocks_I.Reset = '1') THEN
					FSM_STATE <= s0_start;
				ELSE
					FSM_STATE <= FSM_STATE_NEXT;
					SOP_Count <= SOP_Count_next;
					Word_Count <= Word_Count_next;
					EOP_Count <= EOP_Count_next;
					
					dataheader_latched <= dataheader_latched_next;
					Words_total_latched <= Words_total_latched_next;
				END IF;
				
			END IF;
		
		
		
					case FSM_STATE is
			
				when s0_start =>
					IsData_ff_next <= '0';
					Data_ff_next <= x"00000000000000000000";
					
					FIFO_RE_O <= '0';
					SOP_Count_next <= 0;
					Word_Count_next <= 0;
					Words_total_latched_next <= 0;
					dataheader_latched_next <= (others => '0');
					EOP_Count_next <= 0;

					if( (FSM_Clocks_I.System_Counter = x"7") and not FIFO_Is_Empty_I ) then
						FSM_STATE_NEXT <= s1_sop;
					else
						FSM_STATE_NEXT <= s0_start;
					end if;
					
				when s1_sop =>
					FIFO_RE_O <= '0';
					
					IsData_ff_next <= SOP_format(SOP_Count)(GBT_data_word_bitdepth);
					Data_ff_next <= SOP_format(SOP_Count)(GBT_data_word_bitdepth-1 downto 0);
																				
					if(FSM_Clocks_I.System_Counter = x"0") then
						dataheader_latched_next <= FIFO_data_word_I;
						Words_total_latched_next <= to_integer(unsigned(  FIFO_data_word_I(fifo_data_bitdepth-1 downto fifo_data_bitdepth-n_pckt_wrds_bitdepth)  ));
					end if;

																
					if(FSM_Clocks_I.System_Counter = x"7") then
						SOP_Count_next <= SOP_Count + 1;
					else 
						SOP_Count_next <= SOP_Count;
					end if;
					
					if( (FSM_Clocks_I.System_Counter = x"7") and (SOP_Count = nwords_in_SOP-1) ) then
						FSM_STATE_NEXT <= s2_data;
					else
						FSM_STATE_NEXT <= s1_sop;
					end if;
					
				when s2_data =>
					IsData_ff_next <= '1';
					Data_ff_next <= FIFO_data_word_I;
					
					if(FSM_Clocks_I.System_Counter = x"7") then
						Word_Count_next <= Word_Count + 1;
						FIFO_RE_O <= '1';
					else
						Word_Count_next <= Word_Count;
						FIFO_RE_O <= '0';
					end if;
					
					if( (FSM_Clocks_I.System_Counter = x"7") and (Word_Count = Words_total_latched) ) then
						FSM_STATE_NEXT <= s3_eop;
					else
						FSM_STATE_NEXT <= s2_data;
					end if;
					
				when s3_eop =>
					FIFO_RE_O <= '0';
					
					IsData_ff_next <= EOP_format(EOP_Count)(GBT_data_word_bitdepth);
					Data_ff_next <= EOP_format(EOP_Count)(GBT_data_word_bitdepth-1 downto 0);
										
					if(FSM_Clocks_I.System_Counter = x"7") then
						EOP_Count_next <= EOP_Count + 1;
					else 
						EOP_Count_next <= EOP_Count;
					end if;
					
					if( (FSM_Clocks_I.System_Counter = x"7") and (EOP_Count = nwords_in_EOP-1) ) then
							FSM_STATE_NEXT <= s0_start;
					else
						FSM_STATE_NEXT <= s3_eop;
					end if;
			end case;

			
		END PROCESS;
		
		
		-- process (FSM_STATE, FSM_Clocks_I.System_Counter, FIFO_Is_Empty_I, SOP_Count, Word_Count, EOP_format, SOP_format, FIFO_data_word_I, Words_total_latched)
		-- begin
					
			-- case FSM_STATE is
			
				-- when s0_start =>
					-- IsData_ff_next <= '0';
					-- Data_ff_next <= x"00000000000000000000";
					
					-- FIFO_RE_O <= '0';
					-- SOP_Count_next <= 0;
					-- Word_Count_next <= 0;
					-- Words_total_latched_next <= 0;
					-- dataheader_latched_next <= (others => '0');
					-- EOP_Count_next <= 0;

					-- if( (FSM_Clocks_I.System_Counter = x"7") and not FIFO_Is_Empty_I ) then
						-- FSM_STATE_NEXT <= s1_sop;
					-- else
						-- FSM_STATE_NEXT <= s0_start;
					-- end if;
					
				-- when s1_sop =>
					-- FIFO_RE_O <= '0';
					
					-- IsData_ff_next <= SOP_format(SOP_Count)(GBT_data_word_bitdepth);
					-- Data_ff_next <= SOP_format(SOP_Count)(GBT_data_word_bitdepth-1 downto 0);
																				
					-- if(FSM_Clocks_I.System_Counter = x"0") then
						-- dataheader_latched_next <= FIFO_data_word_I;
						-- Words_total_latched_next <= to_integer(unsigned(  FIFO_data_word_I(Orbit_id_bitdepth+BC_id_bitdepth+rx_phase_bitdepth+tdwords_bitdepth-1 downto Orbit_id_bitdepth+BC_id_bitdepth+rx_phase_bitdepth)  ));
					-- end if;

																
					-- if(FSM_Clocks_I.System_Counter = x"7") then
						-- SOP_Count_next <= SOP_Count + 1;
					-- else 
						-- SOP_Count_next <= SOP_Count;
					-- end if;
					
					-- if( (FSM_Clocks_I.System_Counter = x"7") and (SOP_Count = nwords_in_SOP-1) ) then
						-- FSM_STATE_NEXT <= s2_data;
					-- else
						-- FSM_STATE_NEXT <= s1_sop;
					-- end if;
					
				-- when s2_data =>
					-- IsData_ff_next <= '1';
					-- Data_ff_next <= FIFO_data_word_I;
					
					-- if(FSM_Clocks_I.System_Counter = x"7") then
						-- Word_Count_next <= Word_Count + 1;
						-- FIFO_RE_O <= '1';
					-- else
						-- Word_Count_next <= Word_Count;
						-- FIFO_RE_O <= '0';
					-- end if;
					
					-- if( (FSM_Clocks_I.System_Counter = x"7") and (Word_Count = Words_total_latched) ) then
						-- FSM_STATE_NEXT <= s3_eop;
					-- else
						-- FSM_STATE_NEXT <= s2_data;
					-- end if;
					
				-- when s3_eop =>
					-- FIFO_RE_O <= '0';
					
					-- IsData_ff_next <= EOP_format(EOP_Count)(GBT_data_word_bitdepth);
					-- Data_ff_next <= EOP_format(EOP_Count)(GBT_data_word_bitdepth-1 downto 0);
										
					-- if(FSM_Clocks_I.System_Counter = x"7") then
						-- EOP_Count_next <= EOP_Count + 1;
					-- else 
						-- EOP_Count_next <= EOP_Count;
					-- end if;
					
					-- if( (FSM_Clocks_I.System_Counter = x"7") and (EOP_Count = nwords_in_EOP-1) ) then
							-- FSM_STATE_NEXT <= s0_start;
					-- else
						-- FSM_STATE_NEXT <= s3_eop;
					-- end if;
			-- end case;

		
		-- end process;
		

		
end Behavioral;

