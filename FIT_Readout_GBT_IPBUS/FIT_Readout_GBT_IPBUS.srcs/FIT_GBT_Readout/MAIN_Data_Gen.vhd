----------------------------------------------------------------------------------
-- Company: INR RAS
-- Engineer: Finogeev D. A. dmitry-finogeev@yandex.ru
-- 
-- Create Date:    07/11/2017 
-- Design Name: 
-- Module Name:    RXDATA_CLKSync - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision
-- Additional Comments: 
--
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use ieee.numeric_std.all;
use ieee.std_logic_unsigned.all ;

use work.fit_gbt_common_package.all;
use work.fit_gbt_board_package.all;


entity MAIN_Data_Gen is
    Port ( 
		FSM_Clocks_I 		: in FSM_Clocks_type;
		
		FIT_GBT_status_I	: in FIT_GBT_status_type;
		Control_register_I	: in CONTROL_REGISTER_type;
		
		Board_data_I		: in board_data_type;
		
		RX_IsData_I 		: in STD_LOGIC;
		RX_Data_I 			: in std_logic_vector(GBT_data_word_bitdepth-1 downto 0);
		
		Board_data_O		: out board_data_type;
		RX_IsData_O 		: out STD_LOGIC;
		RX_Data_O 			: out std_logic_vector(GBT_data_word_bitdepth-1 downto 0)
	 );
end MAIN_Data_Gen;

architecture Behavioral of MAIN_Data_Gen is

	signal Board_data_gen_ff, Board_data_gen_ff_next	: board_data_type;
	signal RX_Data_gen_ff, RX_Data_gen_ff_next 			: std_logic_vector(GBT_data_word_bitdepth-1 downto 0);
	signal RX_IsData_gen_ff, RX_IsData_gen_ff_next		: STD_LOGIC;
	
	signal EV_ID_counter_set : std_logic;
	signal EV_ID_counter		: std_logic_vector(Orbit_id_bitdepth + BC_id_bitdepth-1 downto 0);
	signal IS_Orbit_trg_counter : std_logic;

	signal gen_counter_ff, gen_counter_ff_next : std_logic_vector(GEN_count_bitdepth-1 downto 0); -- module data generation
	signal packets_counter_ff, packets_counter_ff_next : std_logic_vector(data_word_bitdepth-8-1 downto 0); -- data packets counter
	signal phtrg_counter_ff, phtrg_counter_ff_next : std_logic_vector(GEN_count_bitdepth-1 downto 0); -- physics trigger counter
	
	signal Board_data_gen_var, Board_data_gen_void	: board_data_type;
	signal gen_data_word : std_logic_vector(data_word_bitdepth-8-1 downto 0);

    type readout_trg_type is (trg_idle, trg_SOC, trg_SOT, trg_EOC, trg_EOT);
    signal rd_trg_send_mode, rd_trg_send_mode_next : readout_trg_type;
    signal is_rd_trg_send : std_logic;
    
    signal TRG_physics	: std_logic_vector(Trigger_bitdepth-1 downto 0);
	signal is_phys_trigger_sending : std_logic;
	
    signal is_trigger_sending : std_logic; -- emulating CRU trigger messages
    signal TRG_evid	: std_logic_vector(Trigger_bitdepth-1 downto 0);
    signal TRG_readout_command	: std_logic_vector(Trigger_bitdepth-1 downto 0);
    signal TRG_result	: std_logic_vector(Trigger_bitdepth-1 downto 0);

begin


-- ***************************************************
	Board_data_O <= Board_data_gen_ff 	WHEN (Control_register_I.Data_Gen.usage_generator = use_MAIN_generator)	 ELSE Board_data_I;
	RX_Data_O <= RX_Data_I 				WHEN (Control_register_I.Trigger_Gen.usage_generator = use_NO_generator) ELSE RX_Data_gen_ff;
	RX_IsData_O <= RX_IsData_I 			WHEN (Control_register_I.Trigger_Gen.usage_generator = use_NO_generator) ELSE RX_IsData_gen_ff;
-- ***************************************************

-- Data constants generation *************************
	gen_data_word <= packets_counter_ff;
    Board_data_gen_f: for n_word in 0 to total_data_words-1 generate
		Board_data_gen_void.data_array(n_word) <= (others => '0');
		Board_data_gen_var.data_array(n_word) <= std_logic_vector(to_unsigned(n_word, 8)) & gen_data_word;
    end generate;
	
	Board_data_gen_var.is_data <= '1';
	
	-- for CRU tests 10/17 -------
	--Board_data_gen_var.is_data <= 	'1' WHEN (gen_counter_ff = Control_register_I.Data_Gen.n_cycle_void) ELSE
								--	'1' when (FIT_GBT_status_I.BCIDsync_Mode = mode_SYNC) and ((FIT_GBT_status_I.Trigger_from_CRU and TRG_const_response) /= TRG_const_void) ELSE
	--								'0';
	
	--Board_data_gen_var.Trigger_ID <= FIT_GBT_status_I.Trigger_from_CRU; --for trigger response
	----------------------------
	
	
	Board_data_gen_var.rx_phase <= FIT_GBT_status_I.rx_phase;
	Board_data_gen_var.ORBC_ID <= FIT_GBT_status_I.ORBIT_from_CRU_corrected & FIT_GBT_status_I.BCID_from_CRU_corrected;

	Board_data_gen_void.is_data <= '0';
	Board_data_gen_void.ORBC_ID <= (others => '0');
	--Board_data_gen_void.ORBC_ID <= FIT_GBT_status_I.ORBC_ID_from_CRU_corrected; --for trigger response
	--Board_data_gen_void.Trigger_ID <= (others => '0');
	--Board_data_gen_void.Trigger_ID <= FIT_GBT_status_I.Trigger_from_CRU; --for trigger response
	Board_data_gen_void.rx_phase <= (others => '0');
	--Board_data_gen_void.rx_phase <= FIT_GBT_status_I.rx_phase;
-- ***************************************************


-- BC Counter ==================================================
	BC_counter_datagen_comp : entity work.BC_counter
	port map (
		RESET_I			=> FSM_Clocks_I.Reset,
		DATA_CLK_I		=> FSM_Clocks_I.Data_Clk,
		
		IS_INIT_I		=> EV_ID_counter_set,
		ORBC_ID_INIT_I 	=> (others => '0'),
			
		ORBC_ID_COUNT_O => EV_ID_counter,
		IS_Orbit_trg_O	=> IS_Orbit_trg_counter
	);
-- =============================================================


-- Data ff data clk **********************************
	process (FSM_Clocks_I.Data_Clk)
	begin

		IF(rising_edge(FSM_Clocks_I.Data_Clk) )THEN
			IF (FSM_Clocks_I.Reset = '1') THEN
				Board_data_gen_ff	<= Board_data_gen_void;
				RX_Data_gen_ff 		<= (others => '0');
				RX_IsData_gen_ff	<= '0';
				gen_counter_ff		<= (others => '0');
				packets_counter_ff		<= (others => '0');
				phtrg_counter_ff		<= (others => '0');
				rd_trg_send_mode <= trg_idle;
			ELSE
				Board_data_gen_ff	<= Board_data_gen_ff_next;
				RX_Data_gen_ff		<= RX_Data_gen_ff_next;
				RX_IsData_gen_ff	<= RX_IsData_gen_ff_next;
				gen_counter_ff		<= gen_counter_ff_next;
				packets_counter_ff	<= packets_counter_ff_next;
				phtrg_counter_ff	 <= phtrg_counter_ff_next;
				rd_trg_send_mode    <= rd_trg_send_mode_next;
			END IF;
		END IF;
		
	end process;
-- ***************************************************



-- ***************************************************



---------- Counters ---------------------------------
packets_counter_ff_next <= (others => '0') 	WHEN (FSM_Clocks_I.Reset = '1') ELSE
						(others => '0') 	WHEN (packets_counter_ff = x"ff_ffff_ffff_ffff_ffff") ELSE
						packets_counter_ff 	WHEN (gen_counter_ff <= Control_register_I.Data_Gen.n_cycle_void) ELSE
						packets_counter_ff + 1;
						
						--CRU test 10/17 (counting packets)
--                      packets_counter_ff + 1 WHEN ((FIT_GBT_status_I.Trigger_from_CRU and TRG_const_response) /= TRG_const_void) ELSE
--						packets_counter_ff + 1 WHEN (gen_counter_ff = Control_register_I.Data_Gen.n_cycle_void) ELSE
--						packets_counter_ff;

						
gen_counter_ff_next <= 	(others => '0') WHEN (FSM_Clocks_I.Reset = '1') ELSE
						(others => '0')	WHEN (gen_counter_ff = Control_register_I.Data_Gen.n_cycle_data) ELSE
						gen_counter_ff + 1;
						
phtrg_counter_ff_next <= 	(others => '0') WHEN (FSM_Clocks_I.Reset = '1') ELSE
							(others => '0')	WHEN (phtrg_counter_ff = Control_register_I.Trigger_Gen.trigger_rate) ELSE
							phtrg_counter_ff + 1;
						
						
						

---------- Board data gen ---------------------------
-- CRU test 10/17
-- Board_data_gen_ff_next	<= Board_data_gen_var;

Board_data_gen_ff_next <=	Board_data_gen_void WHEN (FSM_Clocks_I.Reset = '1') ELSE
							Board_data_gen_void WHEN (gen_counter_ff <= Control_register_I.Data_Gen.n_cycle_void) ELSE
							Board_data_gen_var;
							
---------- CRU TX data gen  -------------------------
TRG_result <= TRG_readout_command or TRG_evid or TRG_physics;
is_trigger_sending <= IS_Orbit_trg_counter or is_rd_trg_send or is_phys_trigger_sending;


TRG_evid <=  TRG_const_Orbit WHEN (IS_Orbit_trg_counter = '1') ELSE
            (others => '0');
        
TRG_physics <=  TRG_const_Ph WHEN (phtrg_counter_ff = Control_register_I.Trigger_Gen.trigger_rate) ELSE
            (others => '0');
		
-- RX data
RX_Data_gen_ff_next <=	(others => '0') WHEN (FSM_Clocks_I.Reset = '1') ELSE
						(others => '0') WHEN (is_trigger_sending = '0') ELSE
	EV_ID_counter(Orbit_id_bitdepth + BC_id_bitdepth-1 downto BC_id_bitdepth) & x"0" & EV_ID_counter(BC_id_bitdepth-1 downto 0) & TRG_result;	

RX_IsData_gen_ff_next <=	'0' WHEN (FSM_Clocks_I.Reset = '1') ELSE
							'0' WHEN (is_trigger_sending = '0') ELSE
							'1';
		

-- Event ID counter start
EV_ID_counter_set <=	'1' WHEN (gen_counter_ff < x"0002") and (EV_ID_counter = x"00000000_000") ELSE
						'0';
						
-- Physics trigger emulation
is_phys_trigger_sending <= 	'0' WHEN (FSM_Clocks_I.Reset = '1') ELSE
							'1' WHEN(phtrg_counter_ff = Control_register_I.Trigger_Gen.trigger_rate) ELSE
							'0';

-- Readout trigger send
is_rd_trg_send <=   '1' WHEN (rd_trg_send_mode_next /= rd_trg_send_mode) and (rd_trg_send_mode_next /= trg_idle) ELSE
                    '0';

TRG_readout_command <=  TRG_const_SOT WHEN rd_trg_send_mode_next = trg_SOT ELSE
                        TRG_const_EOT WHEN rd_trg_send_mode_next = trg_EOT ELSE
                        TRG_const_SOC WHEN rd_trg_send_mode_next = trg_SOC ELSE
                        TRG_const_EOC WHEN rd_trg_send_mode_next = trg_EOC ELSE
                        (others => '0');

rd_trg_send_mode_next <= trg_idle WHEN (FSM_Clocks_I.Reset = '1') ELSE
    trg_SOT WHEN ((rd_trg_send_mode = trg_idle) and (Control_register_I.Trigger_Gen.Readout_command = send_SOT)) ELSE
    trg_SOC WHEN ((rd_trg_send_mode = trg_idle) and (Control_register_I.Trigger_Gen.Readout_command = send_SOC)) ELSE
    trg_EOT WHEN ((rd_trg_send_mode = trg_idle) and (Control_register_I.Trigger_Gen.Readout_command = send_EOT)) ELSE
    trg_EOC WHEN ((rd_trg_send_mode = trg_idle) and (Control_register_I.Trigger_Gen.Readout_command = send_EOC)) ELSE
    trg_idle WHEN (Control_register_I.Trigger_Gen.Readout_command = command_off) ELSE
    rd_trg_send_mode;




-- ***************************************************

end Behavioral;

